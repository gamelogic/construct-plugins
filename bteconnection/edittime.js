﻿function GetPluginSettings() {
	return {
		"name": "BTE Connection",		// as appears in 'insert object' dialog, can be changed as long as "id" stays the same
		"id": "BTEConnection",		// this is used to identify this plugin and is saved to the project; never change it
		"version": "1.0",					// (float in x.y format) Plugin version - C2 shows compatibility warnings based on this
		"description": "Enables the use of BTE Database connection to handle player's data", // appears at the bottom of the insert object dialog
		"author": "Gamelogic",
		"help url": "http://www.gamelogic.co.za",
		"category": "BTE Games",			// Prefer to re-use existing categories, but you can set anything here
		"type": "object",				// either "world" (appears in layout and is drawn), else "object"
		"rotatable": false,					// only used when "type" is "world".  Enables an angle property on the object.
		"dependency": "gamesettings.mjs",		// The file hat stores the plugin settings
		"flags": 0						// uncomment lines to enable flags...
			| pf_singleglobal		// exists project-wide, e.g. mouse, keyboard.  "type" must be "object".
		//	| pf_texture			// object has a single texture (e.g. tiled background)
		//	| pf_position_aces		// compare/set/get x, y...
		//	| pf_size_aces			// compare/set/get width, height...
		//	| pf_angle_aces			// compare/set/get angle (recommended that "rotatable" be set to true)
		//	| pf_appearance_aces	// compare/set/get visible, opacity...
		//	| pf_tiling				// adjusts image editor features to better suit tiled images (e.g. tiled background)
		//	| pf_animations			// enables the animations system.  See 'Sprite' for usage
		//	| pf_zorder_aces		// move to top, bottom, layer...
		//  | pf_nosize				// prevent resizing in the editor
		//	| pf_effects			// allow WebGL shader effects to be added
		//  | pf_predraw			// set for any plugin which draws and is not a sprite (i.e. does not simply draw
		// a single non-tiling image the size of the object) - required for effects to work properly
	};
};


// Categories
var kServerUpdatesCategory = "Server updates";
var kPromotionalCodesCategory = "Promotional codes";

var kConfigurationCategory = "Configuration";
var kDataCategory = "Data";
var kLeaderboardsCategory = "Leaderboards";

var kMixPanelCategory = "MixPanel Analytics";

//var kPlayerConsumerCategory = "Player/Consumer";
var kDataRetrievalCategory = "Data retrieval";
var kScratchCategory = "Scratch";
var kSlotsCategory = "Slots";
var kLuckyWheelCategory = "Lucky Wheel";
var kClicker = "Clicker";

var kOthers = "Others";

////////////////////////////////////////
// Parameter types:
// AddNumberParam(label, description [, initial_string = "0"])			// a number
// AddStringParam(label, description [, initial_string = "\"\""])		// a string
// AddAnyTypeParam(label, description [, initial_string = "0"])			// accepts either a number or string
// AddCmpParam(label, description)										// combo with equal, not equal, less, etc.
// AddComboParamOption(text)											// (repeat before "AddComboParam" to add combo items)
// AddComboParam(label, description [, initial_selection = 0])			// a dropdown list parameter
// AddObjectParam(label, description)									// a button to click and pick an object type
// AddLayerParam(label, description)									// accepts either a layer number or name (string)
// AddLayoutParam(label, description)									// a dropdown list with all project layouts
// AddKeybParam(label, description)										// a button to click and press a key (returns a VK)
// AddAnimationParam(label, description)								// a string intended to specify an animation name
// AddAudioFileParam(label, description)								// a dropdown list with all imported project audio files

////////////////////////////////////////
// Conditions

// AddCondition(id,					// any positive integer to uniquely identify this condition
//				flags,				// (see docs) cf_none, cf_trigger, cf_fake_trigger, cf_static, cf_not_invertible,
//									// cf_deprecated, cf_incompatible_with_triggers, cf_looping
//				list_name,			// appears in event wizard list
//				category,			// category in event wizard list
//				display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//				description,		// appears in event wizard dialog when selected
//				script_name);		// corresponding runtime function name

// example				
//AddNumberParam("Number", "Enter a number to test if positive.");
//AddCondition(0, cf_none, "Is number positive", "My category", "{0} is positive", "Description for my condition!", "MyCondition");
AddCondition(
	432,
	cf_trigger,
	"OnFeedbackMissing",
	kServerUpdatesCategory,
	"OnFeedbackMissing",
	"OnFeedbackMissing",
	"OnFeedbackMissing");

AddCondition(
	423,
	cf_trigger,
	"OnCheckinComplete",
	kServerUpdatesCategory,
	"OnCheckinComplete",
	"OnCheckinComplete",
	"OnCheckinComplete");

AddCondition(
	0,
	cf_trigger,
	"Game Credits Updated",
	kServerUpdatesCategory,
	"On Update <i>Game Credits</i>",
	"Triggered when the server responds with new game credits.",
	"OnServerUpdateGameCredits");

AddCondition(
	1,
	cf_trigger,
	"Not Enough Credits",
	kServerUpdatesCategory,
	"On Not Enough Credits To Play",
	"Triggered when the server responds that the player doesn't have enough credits to play the game.",
	"OnNotEnoughCredits");

AddCondition(
	2,
	cf_trigger,
	"BTEConnection Initialized",
	kServerUpdatesCategory,
	"On BTEConnection initialization finish",
	"Triggered when a the BTEConnection has finished initializing (everything is available for use).",
	"OnBTEConnectionInitialized");

AddCondition(
	3,
	cf_trigger,
	"Fatal Error",
	kServerUpdatesCategory,
	"On fatal error",
	"Triggered when an error prevents the game from being playable, usually because of some connection issue.",
	"OnFatalError");

AddCondition(
	4,
	cf_trigger,
	"No Location",
	kServerUpdatesCategory,
	"On not has location",
	"Triggered if the URL does not contain location information.",
	"NoUserLocation");

AddCondition(
	10,
	cf_none,
	"Game Is Playable",
	kOthers,
	"The game is playable",
	"The game can be played because the cost to play it can be covered.",
	"IsGamePlayable");

AddCondition(
	11,
	cf_none,
	"Game Is Web",
	kOthers,
	"The game is web",
	"The game is played from a web browser.",
	"IsWeb");

AddCondition(
	202,
	cf_trigger,
	"Leaderboard Scores",
	kServerUpdatesCategory,
	"On Leaderboard Scores",
	"Triggered when the server responds with leaderboard data",
	"OnLeaderboardScores");

AddCondition(
	301,
	cf_trigger,
	"Request Slots Result",
	kServerUpdatesCategory,
	"On Request Slots Results",
	"Triggered when the server responds with a new result for a slots game",
	"OnRequestSlotsResult");

AddCondition(
	302,
	cf_trigger,
	"Request Scratch Result",
	kServerUpdatesCategory,
	"On Request Scratch Results",
	"Triggered when the server responds with a new result for a scratch game",
	"OnRequestScratchResult");

AddCondition(
	303,
	cf_trigger,
	"Request LuckyWheel Result",
	kServerUpdatesCategory,
	"On Request Slots Results",
	"Triggered when the server responds with a new result for a LuckyWheel game",
	"OnRequestLuckyWheelResult");

AddCondition(
	421,
	cf_trigger,
	"Shows Message",
	kServerUpdatesCategory,
	"On Request Slots Results",
	"Triggered when the server responds with a new result for a LuckyWheel game",
	"OnRequestLuckyWheelResult");

// PROMOTIONAL CODES

//AddCondition(
//	401,
//	cf_trigger,
//	"Code won",
//	kPromotionalCodesCategory,
//	"On Code Won",
//	"Triggered when a code is won",
//	"OnCodeWon");

AddCondition(
	402,
	cf_trigger,
	"Code verified",
	kPromotionalCodesCategory,
	"On Code Verified",
	"Triggered when a code is verified on the server, the result can be retrieved by the expression GetCodeVerificationResult",
	"OnCodeVerified");

AddCondition(
	410,
	cf_trigger,
	"Leaderboard Post Response",
	kPromotionalCodesCategory,
	"On Leaderboards post response",
	"Triggered when the leaderboards responds about the submitted score",
	"OnLeaderboardPostResponse");

// MIX PANEL ANALYTICS CONDITIONS (500)
AddCondition(
	501,
	cf_trigger,
	"MixPanel Initialized",
	kMixPanelCategory,
	"On MixPanel Initialized",
	"Triggered when a the MixPanel analytics plugin is loaded and ready to be used.",
	"OnMixPanelInitialized");

AddCondition(
	704,
	cf_trigger,
	"Clicker Got Data",
	kClicker,
	"Clicker Save Data Recieved",
	"Triggered when the requested clicker save data is recieved.",
	"ClickerDataRecieved");

AddCondition(
	706,
	cf_trigger,
	"Clicker New Game",
	kClicker,
	"Clicker has no saved data",
	"Triggered when the user has no saved data.",
	"ClickerNewGame");

AddCondition(
	707,
	cf_trigger,
	"Clicker Data Error",
	kClicker,
	"Clicker data fetch error",
	"Triggered when there is an issue when getting clicker save data.",
	"ClickerDataError");

AddCondition(
	709,
	cf_trigger,
	"Connection Error Recall",
	kOthers,
	"Connection Error Recall",
	"Triggered when there is an issue when connecting to external servers.",
	"retryFunctionQued");

////////////////////////////////////////
// Actions

// AddAction(id,				// any positive integer to uniquely identify this action
//			 flags,				// (see docs) af_none, af_deprecated
//			 list_name,			// appears in event wizard list
//			 category,			// category in event wizard list
//			 display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//			 description,		// appears in event wizard dialog when selected
//			 script_name);		// corresponding runtime function name

// example
//AddStringParam("Message", "Enter a string to alert.");
//AddAction(actionId++, af_none, "Alert", "My category", "Alert {0}", "Description for my action!", "MyAction");

// Configure the global state for the connection
//AddStringParam("Game Name", "The name of the game with the format GameName");
//AddNumberParam("Game Id", "The id of the game as it's expected by the database");
AddNumberParam("Latitude", "The Player´s GPS position");
AddNumberParam("Longitude", "The Player´s GPS position");
AddAction(
	420,
	af_none,
	"CheckIn",
	kDataCategory,
	"CheckIn",
	"CheckIn",
	"CheckIn");

AddStringParam("statName", "The target statistic name.");
AddAction(
	425,
	af_none,
	"GetStatistic",
	kDataCategory,
	"GetStatistic",
	"GetStatistic",
	"GetStatistic");

AddAction(
	426,
	af_none,
	"GetDailyCredits",
	kDataCategory,
	"GetDailyCredits",
	"GetDailyCredits",
	"GetDailyCredits");

AddStringParam("text", "The feedback text to submit");
AddAction(
	430,
	af_none,
	"SubmitFeedback",
	kDataCategory,
	"SubmitFeedback",
	"SubmitFeedback",
	"SubmitFeedback");

AddAction(
	431,
	af_none,
	"QueryFeedback",
	kDataCategory,
	"QueryFeedback",
	"QueryFeedback",
	"QueryFeedback");

AddAction(
	0,
	af_none,
	"Initialize",
	kConfigurationCategory,
	"Initialize the BTE Connection",
	"Allows to initialize the BTE Connection",
	"Initialize");

AddNumberParam(
	"Credits",
	"The credits that the account is trying to use.");
AddAction(
	100,
	af_deprecated,
	"Use Game Credits",
	kDataCategory,
	"Try to use <b>{0}</b> game credits on the server",
	"Use game credits on the server and trigger update conditions to check if it's valid to use that amount",
	"UseGameCredits");

AddNumberParam(
	"Credits",
	"The credits that the account will set in the server.");

AddAction(
	101,
	af_deprecated,
	"Set Game Credits",
	kDataCategory,
	"Try to set <b>{0}</b> game credits on the server",
	"Set game credits on the server",
	"SetGameCredits");

AddAction(
	102,
	af_none,
	"Hide close button",
	kOthers,
	"Hide the external close button",
	"Hide the external close button",
	"HideCloseButton"
);

AddNumberParam("Score",
	"The scores to be posted for the player.");
// Add a param for the "state"
AddAction(
	202,
	af_none,
	"Post Leaderboard Score",
	kLeaderboardsCategory,
	"Post <b>{0}</b> to leaderboards",
	"Post the specified score to the leaderboards.",
	"PostScore");

AddAction(
	203,
	af_none,
	"Get Leaderboard Scores",
	kLeaderboardsCategory,
	"Get the leaderboards scores",
	"Get the scores for the top 10 in the leaderboards plus the player position.",
	"GetLeaderboardScores");

AddStringParam("DisplayName",
	"The display name for the player, used on leaderboards and other screens.");
AddAction(210,
	af_none,
	"Save DisplayName",
	kDataCategory,
	"Save <b>{0}</b> display name on server",
	"Save the new display name for the player in the database.",
	"SaveDisplayName");




AddNumberParam(
	"Cost",
	"The cost to play Slots");
AddAction(
	301,
	af_none,
	"Request Slots Game",
	kDataCategory,
	"Request Slots game from server for <b>{0}</b> credits",
	"Attempt to get a game result from the server for the Slots game",
	"RequestSlotsResult");

AddNumberParam(
	"Cost",
	"The cost to play Scratch");
AddAction(
	302,
	af_none,
	"Request Scratch Game",
	kDataCategory,
	"Request Scratch game from server for <b>{0}</b> credits",
	"Attempt to get a game result from the server for the Scratch game",
	"RequestScratchResult");

AddNumberParam(
	"Cost",
	"The cost to play LuckyWheel");
AddAction(
	303,
	af_none,
	"Request LuckyWheel Game",
	kDataCategory,
	"Request LuckyWheel game from server for <b>{0}</b> credits",
	"Attempt to get a game result from the server for the LuckyWheel game",
	"RequestLuckyWheelResult");


AddAction(703,
	af_none,
	"Get Clicker Save Data",
	kClicker,
	"Get player clicker save data from the server.",
	"Get Clicker Save Data.",
	"GetClickerData");


AddNumberParam(
	"score",
	"The player's clicker score");
AddStringParam(
	"purchases",
	"The player's purchases count separated by a commma per item");
AddStringParam(
	"upgrades",
	"The player's upgrade levels separated by a comma per item");
AddAction(705,
	af_none,
	"Save Clicker Data",
	kClicker,
	"Save player clicker save data to the server.",
	"Save Clicker Data.",
	"SaveClickerData");

AddAction(708,
	af_none,
	"Retry Connection Error",
	kOthers,
	"Retry the last failed function.",
	"Retry Connection Error.",
	"RecallFailedFunction");

// PROMOTIONAL CODES ACTIONS
AddStringParam("Code",
	"The code the be verified (format first then the server).");
AddAction(401,
	af_none,
	"Verify Code",
	kDataCategory,
	"Verify the code <b>{0}</b> format's first then the validity on the server.",
	"Verifies the code's format and validity.",
	"VerifyCode");


// MIX PANEL ANALYTICS ACTIONS (500)
AddStringParam("Event name",
	"The name of the event.");
AddAction(501,
	af_none,
	"Track Event",
	kMixPanelCategory,
	"Track event <b>{0}</b>.",
	"Tracks an empty event on the mix panel.",
	"Track");

AddStringParam("Event name",
	"The name of the event.");
AddStringParam("Event data",
	"String value associated with this event.");
AddAction(502,
	af_none,
	"Track Event String",
	kMixPanelCategory,
	"Track event <b>{0}</b> with value <b>{1}</b>.",
	"Tracks an event on the mix panel with a string as value.",
	"TrackString");

AddStringParam("Event name",
	"The name of the event.");
AddNumberParam("Event data",
	"Number value associated with this event.");
AddAction(503,
	af_none,
	"Track Event Number",
	kMixPanelCategory,
	"Track event <b>{0}</b> with value <b>{1}</b>.",
	"Tracks an event on the mix panel with a number as value.",
	"TrackNumber");

AddStringParam("Event name",
	"The name of the event.");
AddStringParam("Event data",
	"JSON collection of data as string associated with this event.");
AddAction(504,
	af_none,
	"Track Event JSON",
	kMixPanelCategory,
	"Track event <b>{0}</b> with JSON <b>{1}</b>.",
	"Tracks an event on the mix panel with a JSON associated.",
	"TrackJSON");

////////////////////////////////////////
// Expressions

// AddExpression(id,			// any positive integer to uniquely identify this expression
//				 flags,			// (see docs) ef_none, ef_deprecated, ef_return_number, ef_return_string,
//								// ef_return_any, ef_variadic_parameters (one return flag must be specified)
//				 list_name,		// currently ignored, but set as if appeared in event wizard
//				 category,		// category in expressions panel
//				 exp_name,		// the expression name after the dot, e.g. "foo" for "myobject.foo" - also the runtime function name
//				 description);	// description in expressions panel

// example
//AddExpression(0, ef_return_number, "Leet expression", "My category", "MyExpression", "Return the number 1337.");
//AddExpression(0, ef_return_string, "Create Player Id", kPlayerConsumerCategory, "CreatePlayerId", "Creates a new Player Id with the BTE Connection data");

AddNumberParam("RGB", "RGB");
AddExpression(
	436,
	ef_return_number,
	"GetKombiColor",
	kDataCategory,
	"GetKombiColor",
	"GetKombiColor");

AddExpression(
	434,
	ef_return_string,
	"GetFullGameName",
	kDataCategory,
	"GetFullGameName",
	"GetFullGameName");

AddNumberParam("lightNumber", "Light Number");
AddExpression(
	435,
	ef_return_string,
	"GetLightPosition",
	kDataCategory,
	"GetLightPosition",
	"GetLightPosition");

AddNumberParam("colorGroup", "Color Group");
AddExpression(
	433,
	ef_return_number,
	"GetTextColor",
	kDataCategory,
	"GetTextColor",
	"GetTextColor");

AddExpression(
	428,
	ef_return_string,
	"BrandName",
	kDataCategory,
	"BrandName",
	"BrandName");

AddExpression(
	429,
	ef_return_string,
	"DownloadURL",
	kDataCategory,
	"DownloadURL",
	"DownloadURL");

AddExpression(
	427,
	ef_return_number,
	"GetKombi",
	kDataCategory,
	"GetKombi",
	"GetKombi");

AddExpression(
	424,
	af_none,
	"CheckIn",
	kDataCategory,
	"CheckIn",
	"CheckIn");

AddExpression(
	422,
	ef_return_number,
	"CheckInResult",
	kDataCategory,
	"CheckInResult",
	"CheckInResult");

AddExpression(
	423,
	ef_return_string,
	"CheckInTextResult",
	kDataCategory,
	"CheckInTextResult",
	"CheckInTextResult");

AddExpression(
	100,
	ef_return_number,
	"Get Game Credits",
	kDataRetrievalCategory,
	"GetGameCredits",
	"Returns the current GameCredits value retrieved from the server");

AddExpression(
	203,
	ef_return_number,
	"Slots Result GameCredits Updated",
	kSlotsCategory,
	"SlotsResultGameCredits",
	"Returns the updated gameCredits after playing the Slots");

AddExpression(
	208,
	ef_return_number,
	"Scratch Result GameCredits Updated",
	kScratchCategory,
	"ScratchResultGameCredits",
	"Returns the updated gameCredits after playing the Scratch");

AddExpression(
	700,
	ef_return_number,
	"Clicker Score",
	kClicker,
	"GetClickerScore",
	"Returns the player's saved game score");

AddNumberParam("item", "Item Number");
AddExpression(
	701,
	ef_return_number,
	"Clicker Purchases",
	kClicker,
	"GetClickerPurchases",
	"Returns a string of the player's purchases separated by a comma");

AddNumberParam("item", "Item Number");
AddExpression(
	702,
	ef_return_number,
	"Clicker Upgrades",
	kClicker,
	"GetClickerUpgrades",
	"Returns a string of the player's upgrades separated by a comma");

// Slots results
AddExpression(
	201,
	ef_return_number,
	"Slots Result Credits",
	kSlotsCategory,
	"SlotsResultCredits",
	"Returns the current Slots Result Credits value retrieved from the server");

AddExpression(
	202,
	ef_return_string,
	"Slots Result Prize",
	kSlotsCategory,
	"SlotsResultPrize",
	"Returns the current Slots Result Prize value retrieved from the server");

AddExpression(
	203,
	ef_return_number,
	"Slots Prize ID",
	kSlotsCategory,
	"SlotsPrizeId",
	"Returns different prize id for a variety of prizes");


// Scratch results
AddExpression(
	204,
	ef_return_number,
	"Scratch Result Credits",
	kScratchCategory,
	"ScratchResultCredits",
	"Returns the current Scratch Result Credits value retrieved from the server");

AddExpression(
	205,
	ef_return_string,
	"Scratch Result Prize",
	kScratchCategory,
	"ScratchResultPrize",
	"Returns the current Scratch Result Prize value retrieved from the server");



// Lucky Wheel results
AddExpression(
	206,
	ef_return_number,
	"LuckyWheel Result Credits",
	kLuckyWheelCategory,
	"LuckyWheelResultCredits",
	"Returns the current LuckyWheel Result Credits value retrieved from the server");

AddExpression(
	207,
	ef_return_string,
	"LuckyWheel Result Prize",
	kLuckyWheelCategory,
	"LuckyWheelResultPrize",
	"Returns the current LuckyWheel Result Prize value retrieved from the server");

AddExpression(
	208,
	ef_return_string,
	"LuckyWheel Result Play Again",
	kLuckyWheelCategory,
	"LuckyWheelResultPlayAgain",
	"Returns the current LuckyWheel Result Play Again value retrieved from the server");

AddExpression(
	209,
	ef_return_number,
	"LuckyWheel Result Unlucky",
	kLuckyWheelCategory,
	"LuckyWheelResultUnlucky",
	"Returns the current LuckyWheel Result Unlucky value retrieved from the server");




// Leaderboards
AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddExpression(
	301,
	ef_return_number,
	"Get the position for the main player",
	kDataRetrievalCategory,
	"GetPositionForMainPlayer",
	"Returns the main player leaderboard position");

AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddExpression(
	302,
	ef_return_string,
	"Get the name for the main player",
	kDataRetrievalCategory,
	"GetNameForMainPlayer",
	"Returns the main player leaderboard name");

AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddExpression(
	303,
	ef_return_number,
	"Get the score for the main player",
	kDataRetrievalCategory,
	"GetScoreForMainPlayer",
	"Returns the main player score");

AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddNumberParam("playerIndex", "The array index of the player.");
AddExpression(
	304,
	ef_return_number,
	"Get the position for the player with the given index",
	kDataRetrievalCategory,
	"GetPositionForPlayer",
	"Returns the position of the player with the given array index");

AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddNumberParam("playerIndex", "The array index of the player.");
AddExpression(
	305,
	ef_return_number,
	"Get the score for the player with the given index",
	kDataRetrievalCategory,
	"GetScoreForPlayer",
	"Returns the score for the player with the given index");

AddNumberParam("leaderboardType", "0 for local, 1 for global.");
AddNumberParam("playerIndex", "The array index of the player.");
AddExpression(
	306,
	ef_return_string,
	"Get the name the player with the given index",
	kDataRetrievalCategory,
	"GetNameForPlayer",
	"Returns the name of the player with the given index");

AddStringParam("statName", "The Name of the target statistic.");
AddExpression(
	485,
	ef_return_number,
	"Get the statistic number",
	kDataRetrievalCategory,
	"GetPlayerStatistic",
	"Returns the number of that statistic");

AddExpression(
	307,
	ef_return_string,
	"Gets the main player display name",
	kDataRetrievalCategory,
	"GetMainPlayerDisplayName",
	"Returns the display name of the main player");

AddExpression(
	308,
	ef_return_string,
	"Gets the main player us state",
	kDataRetrievalCategory,
	"GetMainPlayerUSState",
	"Returns the US state detected for the main player");

AddExpression(
	309,
	ef_return_string,
	"Gets the current set up Game Name",
	kDataRetrievalCategory,
	"GetGameName",
	"Returns the Game Name that was set up on the plugin object properties");

AddExpression(
	310,
	ef_return_string,
	"Gets the current game version",
	kDataRetrievalCategory,
	"GetVersionNumber",
	"Returns the Game Version that was set up on the plugin object properties");


/*AddExpression(
	307, 
	ef_return_number, 
	"Get the credits received in last play.", 
	kDataRetrievalCategory, 
	"ResultCredits", 
	"Returns the credits received in last play");
	
AddExpression(
	308, 
	ef_return_number, 
	"Get the prize received in the last play", 
	kDataRetrievalCategory, 
	"ResultPrize", 
	"Returns the prize received in the last play");*/

// PROMOTIONAL CODES EXPRESSIONS
AddExpression(
	401,
	ef_return_number,
	"Get the total amount of codes that the player currently has",
	kPromotionalCodesCategory,
	"GetGameToLifeCodesTotal",
	"Returns total amount of codes for the player");

AddNumberParam("index", "index of the code to retrieve between 0 and GetGameToLifeCodesTotal - 1");
AddExpression(
	402,
	ef_return_string,
	"Get the code at the specified index in the list of game to life codes for the player",
	kPromotionalCodesCategory,
	"GetGameToLifeCode",
	"Returns the code at the specified index");

AddExpression(
	403,
	ef_return_string,
	"Get the result for the last code verification, that is OK, CLAIMED, INVALID, EMPTY or ERROR",
	kPromotionalCodesCategory,
	"GetCodeVerificationResult",
	"Returns the result for the last code verification");

AddExpression(
	404,
	ef_return_string,
	"Get the result message for the last code verification, this is a descriptive message",
	kPromotionalCodesCategory,
	"GetCodeVerificationMessage",
	"Returns the result message for the last code verification");

AddNumberParam("index", "index of the code to retrieve between 0 and GetGameToLifeCodesTotal - 1");
AddExpression(
	405,
	ef_return_string,
	"",
	kPromotionalCodesCategory,
	"IsGameToLifeCodeNew",
	"");

AddExpression(
	410,
	ef_return_string,
	"",
	kPromotionalCodesCategory,
	"LastLeaderboardPostHasCode",
	"");

///////////////////////////////////////
ACESDone();

////////////////////////////////////////
// Array of property grid properties for this plugin
// new cr.Property(ept_integer,		name,	initial_value,	description)		// an integer value
// new cr.Property(ept_float,		name,	initial_value,	description)		// a float value
// new cr.Property(ept_text,		name,	initial_value,	description)		// a string
// new cr.Property(ept_color,		name,	initial_value,	description)		// a color dropdown
// new cr.Property(ept_font,		name,	"Arial,-16", 	description)		// a font with the given face name and size
// new cr.Property(ept_combo,		name,	"Item 1",		description, "Item 1|Item 2|Item 3")	// a dropdown list (initial_value is string of initially selected item)
// new cr.Property(ept_link,		name,	link_text,		description, "firstonly")		// has no associated value; simply calls "OnPropertyChanged" on click

var property_list = [
	new cr.Property(ept_text, "Game Name", "", "The name of the game InThisFormat."),
	new cr.Property(ept_text, "Server Environment", "", "The server environment PDR or ENV."),
	new cr.Property(ept_text, "Secret Key", "", "The secret key for the server."),
	new cr.Property(ept_text, "Game Version", "", "The game's current version number."),
	//new cr.Property(ept_link, "Localhost", "http://localhost/highscores/", "The localhost URL"),
	//new cr.Property(ept_link, "Gamelogic", "http://gamelogic.co.za/games/highscores/", "The gamelogic URL"),
	//new cr.Property(ept_text, "Server Path", "http://gamelogic.co.za/games/highscores/", "The path to the server interface for the highscores")
];

// Called by IDE when a new object type is to be created
function CreateIDEObjectType() {
	return new IDEObjectType();
}

// Class representing an object type in the IDE
function IDEObjectType() {
	assert2(this instanceof arguments.callee, "Constructor called as a function");
}

// Called by IDE when a new object instance of this type is to be created
IDEObjectType.prototype.CreateInstance = function (instance) {
	return new IDEInstance(instance);
}

// Class representing an individual instance of an object in the IDE
function IDEInstance(instance, type) {
	assert2(this instanceof arguments.callee, "Constructor called as a function");

	// Save the constructor parameters
	this.instance = instance;
	this.type = type;

	// Set the default property values from the property table
	this.properties = {};

	for (var i = 0; i < property_list.length; i++) {
		this.properties[property_list[i].name] = property_list[i].initial_value;
	}

	// Plugin-specific variables
	// this.myValue = 0...
}

// Called when inserted via Insert Object Dialog for the first time
IDEInstance.prototype.OnInserted = function () {
}

// Called when double clicked in layout
IDEInstance.prototype.OnDoubleClicked = function () {
}

// Called after a property has been changed in the properties bar
IDEInstance.prototype.OnPropertyChanged = function (property_name) {
}

// For rendered objects to load fonts or textures
IDEInstance.prototype.OnRendererInit = function (renderer) {
}

// Called to draw self in the editor if a layout object
IDEInstance.prototype.Draw = function (renderer) {
}

// For rendered objects to release fonts or textures
IDEInstance.prototype.OnRendererReleased = function (renderer) {
}