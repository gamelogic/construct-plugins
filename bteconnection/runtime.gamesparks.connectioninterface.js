///////////////////////////////////////
// CONNECTION INTERFACE FOR GAMESPARKS

/////////////////
// Custom calls

RemoteConnection = function ()
{

};

RemoteConnection.prototype.GetName = function ()
{
	return "GameSparks Connection";
}

RemoteConnection.prototype.Init = function ()
{
	gamesparks.initPreview({
		key: "f303384rP0NU",
		onNonce: this.onGameSparksNonce,
		onInit: () => this.onGameSparksInit(),
		onMessage: (message) => {}//{ console.log(JSON.stringify(message)); }
	});
}

RemoteConnection.prototype.addGameCredits = function (amount, onResponse)
{
	var request = {};
	request["eventKey"] = "addGameCredits";
	request["amount"] = amount;
	gamesparks.sendWithData("LogEventRequest", request, onResponse);
}

RemoteConnection.prototype.requestSlotsResult = function (cost, onResponse)
{
	var request = {};
	request["eventKey"] = "requestSlotsResult";
	request["cost"] = cost;
	gamesparks.sendWithData("LogEventRequest", request, onResponse);
}

RemoteConnection.prototype.postScore = function (score, usstate, onResponse)
{
	var request = {};
	request["eventKey"] = "postScore";
	request["score"] = score;
	gamesparks.sendWithData("LogEventRequest", request,
		(r1) =>
		{
			var request = {};
			request["eventKey"] = "postScoreUSState";
			request["score"] = score;
			request["usstate"] = usstate;
			gamesparks.sendWithData("LogEventRequest", request,
				(r2) =>
				{
					onResponse(r1, r2);
				}
			);
		}
	);
}

RemoteConnection.prototype.getLeaderboardScores = function (usState, onResponse)
{
	// GLOBAL
	var request = {};
	request["entryCount"] = 10;
	request["leaderboardShortCode"] = "globalLeaderboard";
	gamesparks.sendWithData("LeaderboardDataRequest", request,
		(r1) =>
		{


			// LOCAL
			var request2 = {};
			request2["entryCount"] = 10;
			request2["leaderboardShortCode"] = "localLeaderboard.usstate." + usState;
			gamesparks.sendWithData("LeaderboardDataRequest", request2,
				(r2) =>
				{


					console.log("$$$ leaderboard top 10: " + JSON.stringify(r1, null, 3));
					// PLAYER
					var request3 = {};
					request3["leaderboards"] = ["globalLeaderboard", "localLeaderboard.usstate." + usState];
					gamesparks.sendWithData("GetLeaderboardEntriesRequest", request3,
						(r3) =>
						{


							console.log("$$$ leaderboard me: " + JSON.stringify(r3, null, 3));

							var globalTop10 = r1["data"] ? r1["data"] : [];
							var globalPlayer = r3["globalLeaderboard"] ? r3["globalLeaderboard"] : {};

							var localTop10 = r2["data"] ? r2["data"] : [];
							var localPlayer = r3["localLeaderboard.usstate." + usState] ? r3["localLeaderboard.usstate." + usState] : {};

							onResponse(
							{
								global: {
									player: globalPlayer,
									top10: globalTop10
								},
								local: {
									player: localPlayer,
									top10: localTop10
								}
							});
						}
					);
				}
			);
		}
	);
}

// END OF CONNECTION INTERFACE
//////////////////////////////