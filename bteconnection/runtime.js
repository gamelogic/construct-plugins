﻿/*function BTEConnectionGameID() {
	return "EAE4";
}

function BTEConnectionBrandName() {
	return "Circle K";
}


function BTEConnectionDownloadURL() {
	return "https://rfr59.app.goo.gl/ppJJ";
}*/

/*var BTEConnectionGameID = "B6B6";
var BTEConnectionBrandName = "Spin and Win";
var BTEConnectionDownloadURL = "http://plinq.co";*/

var BTEConnectionGameID = "EAE4";
var BTEConnectionBrandName = "Circle K";
var BTEConnectionDownloadURL = "https://rfr59.app.goo.gl/ppJJ";
var GameName = "Spin 2 Win";
var KombiColor = [103,16,72];
var TextColors = [
	[255,255,255], 	//Dialog Text
	[255,255,255],	//Dialog Button Text
	[21,103,56],	//Main Menu Button Text
	[102,0,0],		//Score Text
	[252,248,182],	//Panel Text
	[102,0,0],		//Heading Text
	[21,103,56]		//Heading2 Text
];
var LightPosition = [
	[231,377],
	[334,432],
	[462,416],
	[540,382],
	[647,428],
	[815,423],
	[909,391],
	[1019,434],
	[1163,430],
	[1232,397],
	[1323,432],
	[1434,427],
	[1504,395],
	[1581,425],
	[1681,430],
	[1753,384]
];
/////////////////////////////
// BEGINING OF PLAYFAB PLUGIN

/// <reference path="../typings/PlayFab/PlayFabClientApi.d.ts" />

var PlayFab = typeof PlayFab != "undefined" ? PlayFab : {};
var PlayFabRevisionSelection = (location.hostname === "localhost" || location.hostname === "127.0.0.1") ? "Latest" : "Live";

var retryFunction;

if (!PlayFab.settings) {
	PlayFab.settings = {
		// You must set this value for PlayFabSdk to work properly (Found in the Game Manager for your title, at the PlayFab Website)
		titleId: 0,

		// For security reasons you must never expose this value to the client or players - 
		// You must set this value for Server-APIs to work properly (Found in the Game Manager for your title, at the PlayFab Website)
		developerSecretKey: "",

		// ADs?
		advertisingIdType: null,
		advertisingIdValue: null,
		// disableAdvertising is provided for completeness, but changing it is not suggested
		// Disabling this may prevent your advertising-related PlayFab marketplace partners from working correctly
		disableAdvertising: false,
		AD_TYPE_IDFA: "Idfa",
		AD_TYPE_ANDROID_ID: "Adid"
	}
}

if (!PlayFab._internalSettings) {
	PlayFab._internalSettings = {
		sessionTicket: null,
		productionServerUrl: ".playfabapi.com",

		GetServerUrl: function () {
			return "https://" + PlayFab.settings.titleId + PlayFab._internalSettings.productionServerUrl;
		},

		ExecuteRequest: function (completeUrl, data, authkey, authValue, callback) {
			if (callback != null && typeof (callback) != "function")
				throw "Callback must be null of a function";

			if (data == null)
				data = {};

			var startTime = new Date();
			var requestBody = JSON.stringify(data);

			var xhr = new XMLHttpRequest();
			// window.console.log("URL: " + completeUrl);
			xhr.open("POST", completeUrl, true);

			xhr.setRequestHeader('Content-Type', 'application/json');

			if (authkey != null)
				xhr.setRequestHeader(authkey, authValue);

			xhr.setRequestHeader('X-PlayFabSDK', "JavaScriptSDK-" + PlayFab._internalSettings.sdkVersion);

			xhr.onloadend = function () {
				if (callback == null)
					return;

				var result;
				try {
					// window.console.log("parsing json result: " + xhr.responseText);
					result = JSON.parse(xhr.responseText);
				} catch (e) {
					result = {
						code: 503, // Service Unavailable
						status: "Service Unavailable",
						error: "Connection error",
						errorCode: 2, // PlayFabErrorCode.ConnectionError
						errorMessage: xhr.responseText
					};
				}

				result.CallBackTimeMS = new Date() - startTime;

				if (result.code === 200)
					callback(result, null);
				else
					callback(null, result);
			}

			xhr.onerror = function () {
				if (callback == null)
					return;

				var result;
				try {
					result = JSON.parse(xhr.responseText);
				} catch (e) {
					result = {
						code: 503, // Service Unavailable
						status: "Service Unavailable",
						error: "Connection error",
						errorCode: 2, // PlayFabErrorCode.ConnectionError
						errorMessage: xhr.responseText
					};
				}

				result.CallBackTimeMS = new Date() - startTime;
				callback(null, result);
			}

			xhr.send(requestBody);
		}
	}
}

PlayFab.buildIdentifier = "jbuild_javascriptsdk_1";
PlayFab.sdkVersion = "1.2.170313";

PlayFab.ClientApi =
	{
		IsClientLoggedIn: function () { return PlayFab._internalSettings.sessionTicket != null && PlayFab._internalSettings.sessionTicket.length > 0; },
		GetPhotonAuthenticationToken: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPhotonAuthenticationToken", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetWindowsHelloChallenge: function (request, callback) { PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetWindowsHelloChallenge", request, null, null, callback); },
		LinkWindowsHello: function (request, callback) { PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkWindowsHello", request, null, null, callback); },
		LoginWithAndroidDeviceID: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithAndroidDeviceID", request, null, null, overloadCallback); },
		LoginWithCustomID: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithCustomID", request, null, null, overloadCallback); },
		LoginWithEmailAddress: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithEmailAddress", request, null, null, overloadCallback); },
		LoginWithFacebook: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithFacebook", request, null, null, overloadCallback); },
		LoginWithGameCenter: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithGameCenter", request, null, null, overloadCallback); },
		LoginWithGoogleAccount: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithGoogleAccount", request, null, null, overloadCallback); },
		LoginWithIOSDeviceID: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithIOSDeviceID", request, null, null, overloadCallback); },
		LoginWithKongregate: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithKongregate", request, null, null, overloadCallback); },
		LoginWithPlayFab: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithPlayFab", request, null, null, overloadCallback); },
		LoginWithSteam: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithSteam", request, null, null, overloadCallback); },
		LoginWithTwitch: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithTwitch", request, null, null, overloadCallback); },
		LoginWithWindowsHello: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LoginWithWindowsHello", request, null, null, overloadCallback); },
		RegisterPlayFabUser: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RegisterPlayFabUser", request, null, null, overloadCallback); },
		RegisterWithWindowsHello: function (request, callback) { request.TitleId = PlayFab.settings.titleId != null ? PlayFab.settings.titleId : request.TitleId; if (request.TitleId == null) throw "Must be have PlayFab.settings.titleId set to call this method"; var overloadCallback = function (result, error) { if (result != null && result.data.SessionTicket != null) { PlayFab._internalSettings.sessionTicket = result.data.SessionTicket; PlayFab.ClientApi._MultiStepClientLogin(result.data.SettingsForUser.NeedsAttribution); } if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RegisterWithWindowsHello", request, null, null, overloadCallback); },
		UnlinkWindowsHello: function (request, callback) { PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkWindowsHello", request, null, null, callback); },
		AddGenericID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AddGenericID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AddUsernamePassword: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AddUsernamePassword", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetAccountInfo: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetAccountInfo", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayerCombinedInfo: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerCombinedInfo", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromFacebookIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromFacebookIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromGameCenterIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromGameCenterIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromGenericIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromGenericIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromGoogleIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromGoogleIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromKongregateIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromKongregateIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromSteamIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromSteamIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayFabIDsFromTwitchIDs: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayFabIDsFromTwitchIDs", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkAndroidDeviceID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkAndroidDeviceID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkCustomID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkCustomID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkFacebookAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkFacebookAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkGameCenterAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkGameCenterAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkGoogleAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkGoogleAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkIOSDeviceID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkIOSDeviceID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkKongregate: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkKongregate", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkSteamAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkSteamAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		LinkTwitch: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/LinkTwitch", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RemoveGenericID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RemoveGenericID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ReportPlayer: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ReportPlayer", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		SendAccountRecoveryEmail: function (request, callback) { PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/SendAccountRecoveryEmail", request, null, null, callback); },
		UnlinkAndroidDeviceID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkAndroidDeviceID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkCustomID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkCustomID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkFacebookAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkFacebookAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkGameCenterAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkGameCenterAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkGoogleAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkGoogleAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkIOSDeviceID: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkIOSDeviceID", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkKongregate: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkKongregate", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkSteamAccount: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkSteamAccount", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlinkTwitch: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlinkTwitch", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateAvatarUrl: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateAvatarUrl", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateUserTitleDisplayName: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateUserTitleDisplayName", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetFriendLeaderboard: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetFriendLeaderboard", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetFriendLeaderboardAroundPlayer: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetFriendLeaderboardAroundPlayer", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetLeaderboard: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetLeaderboard", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetLeaderboardAroundPlayer: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetLeaderboardAroundPlayer", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayerStatistics: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerStatistics", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayerStatisticVersions: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerStatisticVersions", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetUserData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetUserData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetUserPublisherData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetUserPublisherData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetUserPublisherReadOnlyData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetUserPublisherReadOnlyData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetUserReadOnlyData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetUserReadOnlyData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdatePlayerStatistics: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdatePlayerStatistics", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateUserData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateUserData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateUserPublisherData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateUserPublisherData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCatalogItems: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCatalogItems", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPublisherData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPublisherData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetStoreItems: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetStoreItems", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetTime: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetTime", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetTitleData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetTitleData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetTitleNews: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetTitleNews", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AddUserVirtualCurrency: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AddUserVirtualCurrency", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ConfirmPurchase: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ConfirmPurchase", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ConsumeItem: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ConsumeItem", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCharacterInventory: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCharacterInventory", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPurchase: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPurchase", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetUserInventory: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetUserInventory", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		PayForPurchase: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/PayForPurchase", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		PurchaseItem: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/PurchaseItem", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RedeemCoupon: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RedeemCoupon", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		StartPurchase: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/StartPurchase", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		SubtractUserVirtualCurrency: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/SubtractUserVirtualCurrency", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlockContainerInstance: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlockContainerInstance", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UnlockContainerItem: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UnlockContainerItem", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AddFriend: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AddFriend", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetFriendsList: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetFriendsList", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RemoveFriend: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RemoveFriend", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		SetFriendTags: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/SetFriendTags", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RegisterForIOSPushNotification: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RegisterForIOSPushNotification", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RestoreIOSPurchases: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RestoreIOSPurchases", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ValidateIOSReceipt: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ValidateIOSReceipt", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCurrentGames: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCurrentGames", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetGameServerRegions: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetGameServerRegions", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		Matchmake: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/Matchmake", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		StartGame: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/StartGame", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AndroidDevicePushNotificationRegistration: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AndroidDevicePushNotificationRegistration", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ValidateGooglePlayPurchase: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ValidateGooglePlayPurchase", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		WriteCharacterEvent: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/WriteCharacterEvent", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		WritePlayerEvent: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/WritePlayerEvent", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		WriteTitleEvent: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/WriteTitleEvent", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AddSharedGroupMembers: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AddSharedGroupMembers", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		CreateSharedGroup: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/CreateSharedGroup", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetSharedGroupData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetSharedGroupData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		RemoveSharedGroupMembers: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/RemoveSharedGroupMembers", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateSharedGroupData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateSharedGroupData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ExecuteCloudScript: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ExecuteCloudScript", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetContentDownloadUrl: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetContentDownloadUrl", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetAllUsersCharacters: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetAllUsersCharacters", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCharacterLeaderboard: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCharacterLeaderboard", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCharacterStatistics: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCharacterStatistics", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetLeaderboardAroundCharacter: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetLeaderboardAroundCharacter", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetLeaderboardForUserCharacters: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetLeaderboardForUserCharacters", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GrantCharacterToUser: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GrantCharacterToUser", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateCharacterStatistics: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateCharacterStatistics", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCharacterData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCharacterData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetCharacterReadOnlyData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetCharacterReadOnlyData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		UpdateCharacterData: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/UpdateCharacterData", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ValidateAmazonIAPReceipt: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ValidateAmazonIAPReceipt", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AcceptTrade: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AcceptTrade", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		CancelTrade: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/CancelTrade", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayerTrades: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerTrades", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetTradeStatus: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetTradeStatus", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		OpenTrade: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/OpenTrade", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		AttributeInstall: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; var overloadCallback = function (result, error) {			/* Modify advertisingIdType:  Prevents us from sending the id multiple times, and allows automated tests to determine id was sent successfully */			PlayFab.settings.advertisingIdType += "_Successful"; if (callback != null && typeof (callback) == "function") callback(result, error); }; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/AttributeInstall", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, overloadCallback); },
		GetPlayerSegments: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerSegments", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		GetPlayerTags: function (request, callback) { if (PlayFab._internalSettings.sessionTicket == null) throw "Must be logged in to call this method"; PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/GetPlayerTags", request, "X-Authorization", PlayFab._internalSettings.sessionTicket, callback); },
		ValidateWindowsStoreReceipt: function (request, callback) { PlayFab._internalSettings.ExecuteRequest(PlayFab._internalSettings.GetServerUrl() + "/Client/ValidateWindowsStoreReceipt", request, null, null, callback); },

		_MultiStepClientLogin: function (needsAttribution) {
			if (needsAttribution && !PlayFab.settings.disableAdvertising && PlayFab.settings.advertisingIdType !== null && PlayFab.settings.advertisingIdValue !== null) {
				var request = {};
				if (PlayFab.settings.advertisingIdType === PlayFab.settings.AD_TYPE_IDFA)
					request.Idfa = PlayFab.settings.advertisingIdValue;
				else if (PlayFab.settings.advertisingIdType === PlayFab.settings.AD_TYPE_ANDROID_ID)
					request.Adid = PlayFab.settings.advertisingIdValue;
				else
					return;
				PlayFab.ClientApi.AttributeInstall(request, null);
			}
		}
	};

var PlayFabClientSDK = PlayFab.ClientApi;

// END OF PLAYFAB PLUGIN
////////////////////////


///////////////////////////////
// BEGINNING OF MIXPANEL PLUGIN

/*
(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
*/

// END OF MIXPANEL PLUGIN
/////////////////////////


//////////////////////////
//Beginning of Rollbar plugin
var _rollbarConfig = {
	accessToken: "0042c7a0cafc463bb57c4fd56f0f4d24",
	captureUncaught: true,
	captureUnhandledRejections: true,
	payload: {
		environment: "production"
	}
};
// Rollbar Snippet
!function (r) { function o(n) { if (e[n]) return e[n].exports; var t = e[n] = { exports: {}, id: n, loaded: !1 }; return r[n].call(t.exports, t, t.exports, o), t.loaded = !0, t.exports } var e = {}; return o.m = r, o.c = e, o.p = "", o(0) }([function (r, o, e) { "use strict"; var n = e(1), t = e(4); _rollbarConfig = _rollbarConfig || {}, _rollbarConfig.rollbarJsUrl = _rollbarConfig.rollbarJsUrl || "https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.5.2/rollbar.min.js", _rollbarConfig.async = void 0 === _rollbarConfig.async || _rollbarConfig.async; var a = n.setupShim(window, _rollbarConfig), l = t(_rollbarConfig); window.rollbar = n.Rollbar, a.loadFull(window, document, !_rollbarConfig.async, _rollbarConfig, l) }, function (r, o, e) { "use strict"; function n(r) { return function () { try { return r.apply(this, arguments) } catch (r) { try { console.error("[Rollbar]: Internal error", r) } catch (r) { } } } } function t(r, o) { this.options = r, this._rollbarOldOnError = null; var e = s++; this.shimId = function () { return e }, "undefined" != typeof window && window._rollbarShims && (window._rollbarShims[e] = { handler: o, messages: [] }) } function a(r, o) { if (r) { var e = o.globalAlias || "Rollbar"; if ("object" == typeof r[e]) return r[e]; r._rollbarShims = {}, r._rollbarWrappedError = null; var t = new p(o); return n(function () { o.captureUncaught && (t._rollbarOldOnError = r.onerror, i.captureUncaughtExceptions(r, t, !0), i.wrapGlobals(r, t, !0)), o.captureUnhandledRejections && i.captureUnhandledRejections(r, t, !0); var n = o.autoInstrument; return o.enabled !== !1 && (void 0 === n || n === !0 || "object" == typeof n && n.network) && r.addEventListener && (r.addEventListener("load", t.captureLoad.bind(t)), r.addEventListener("DOMContentLoaded", t.captureDomContentLoaded.bind(t))), r[e] = t, t })() } } function l(r) { return n(function () { var o = this, e = Array.prototype.slice.call(arguments, 0), n = { shim: o, method: r, args: e, ts: new Date }; window._rollbarShims[this.shimId()].messages.push(n) }) } var i = e(2), s = 0, d = e(3), c = function (r, o) { return new t(r, o) }, p = function (r) { return new d(c, r) }; t.prototype.loadFull = function (r, o, e, t, a) { var l = function () { var o; if (void 0 === r._rollbarDidLoad) { o = new Error("rollbar.js did not load"); for (var e, n, t, l, i = 0; e = r._rollbarShims[i++];)for (e = e.messages || []; n = e.shift();)for (t = n.args || [], i = 0; i < t.length; ++i)if (l = t[i], "function" == typeof l) { l(o); break } } "function" == typeof a && a(o) }, i = !1, s = o.createElement("script"), d = o.getElementsByTagName("script")[0], c = d.parentNode; s.crossOrigin = "", s.src = t.rollbarJsUrl, e || (s.async = !0), s.onload = s.onreadystatechange = n(function () { if (!(i || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState)) { s.onload = s.onreadystatechange = null; try { c.removeChild(s) } catch (r) { } i = !0, l() } }), c.insertBefore(s, d) }, t.prototype.wrap = function (r, o, e) { try { var n; if (n = "function" == typeof o ? o : function () { return o || {} }, "function" != typeof r) return r; if (r._isWrap) return r; if (!r._rollbar_wrapped && (r._rollbar_wrapped = function () { e && "function" == typeof e && e.apply(this, arguments); try { return r.apply(this, arguments) } catch (e) { var o = e; throw o && ("string" == typeof o && (o = new String(o)), o._rollbarContext = n() || {}, o._rollbarContext._wrappedSource = r.toString(), window._rollbarWrappedError = o), o } }, r._rollbar_wrapped._isWrap = !0, r.hasOwnProperty)) for (var t in r) r.hasOwnProperty(t) && (r._rollbar_wrapped[t] = r[t]); return r._rollbar_wrapped } catch (o) { return r } }; for (var u = "log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,captureEvent,captureDomContentLoaded,captureLoad".split(","), f = 0; f < u.length; ++f)t.prototype[u[f]] = l(u[f]); r.exports = { setupShim: a, Rollbar: p } }, function (r, o) { "use strict"; function e(r, o, e) { if (r) { var t; if ("function" == typeof o._rollbarOldOnError) t = o._rollbarOldOnError; else if (r.onerror) { for (t = r.onerror; t._rollbarOldOnError;)t = t._rollbarOldOnError; o._rollbarOldOnError = t } var a = function () { var e = Array.prototype.slice.call(arguments, 0); n(r, o, t, e) }; e && (a._rollbarOldOnError = t), r.onerror = a } } function n(r, o, e, n) { r._rollbarWrappedError && (n[4] || (n[4] = r._rollbarWrappedError), n[5] || (n[5] = r._rollbarWrappedError._rollbarContext), r._rollbarWrappedError = null), o.handleUncaughtException.apply(o, n), e && e.apply(r, n) } function t(r, o, e) { if (r) { "function" == typeof r._rollbarURH && r._rollbarURH.belongsToShim && r.removeEventListener("unhandledrejection", r._rollbarURH); var n = function (r) { var e, n, t; try { e = r.reason } catch (r) { e = void 0 } try { n = r.promise } catch (r) { n = "[unhandledrejection] error getting `promise` from event" } try { t = r.detail, !e && t && (e = t.reason, n = t.promise) } catch (r) { } e || (e = "[unhandledrejection] error getting `reason` from event"), o && o.handleUnhandledRejection && o.handleUnhandledRejection(e, n) }; n.belongsToShim = e, r._rollbarURH = n, r.addEventListener("unhandledrejection", n) } } function a(r, o, e) { if (r) { var n, t, a = "EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(","); for (n = 0; n < a.length; ++n)t = a[n], r[t] && r[t].prototype && l(o, r[t].prototype, e) } } function l(r, o, e) { if (o.hasOwnProperty && o.hasOwnProperty("addEventListener")) { for (var n = o.addEventListener; n._rollbarOldAdd && n.belongsToShim;)n = n._rollbarOldAdd; var t = function (o, e, t) { n.call(this, o, r.wrap(e), t) }; t._rollbarOldAdd = n, t.belongsToShim = e, o.addEventListener = t; for (var a = o.removeEventListener; a._rollbarOldRemove && a.belongsToShim;)a = a._rollbarOldRemove; var l = function (r, o, e) { a.call(this, r, o && o._rollbar_wrapped || o, e) }; l._rollbarOldRemove = a, l.belongsToShim = e, o.removeEventListener = l } } r.exports = { captureUncaughtExceptions: e, captureUnhandledRejections: t, wrapGlobals: a } }, function (r, o) { "use strict"; function e(r, o) { this.impl = r(o, this), this.options = o, n(e.prototype) } function n(r) { for (var o = function (r) { return function () { var o = Array.prototype.slice.call(arguments, 0); if (this.impl[r]) return this.impl[r].apply(this.impl, o) } }, e = "log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId,captureEvent,captureDomContentLoaded,captureLoad".split(","), n = 0; n < e.length; n++)r[e[n]] = o(e[n]) } e.prototype._swapAndProcessMessages = function (r, o) { this.impl = r(this.options); for (var e, n, t; e = o.shift();)n = e.method, t = e.args, this[n] && "function" == typeof this[n] && ("captureDomContentLoaded" === n || "captureLoad" === n ? this[n].apply(this, [t[0], e.ts]) : this[n].apply(this, t)); return this }, r.exports = e }, function (r, o) { "use strict"; r.exports = function (r) { return function (o) { if (!o && !window._rollbarInitialized) { r = r || {}; for (var e, n, t = r.globalAlias || "Rollbar", a = window.rollbar, l = function (r) { return new a(r) }, i = 0; e = window._rollbarShims[i++];)n || (n = e.handler), e.handler._swapAndProcessMessages(l, e.messages); window[t] = n, window._rollbarInitialized = !0 } } } }]);
// End Rollbar Snippet

//////////////////////////
//End of Rollbar plugin


/////////////////////////////////////////////////////////////////////////////////
// Encryption methods

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS = CryptoJS || function (h, s) {
	var f = {}, g = f.lib = {}, q = function () { }, m = g.Base = { extend: function (a) { q.prototype = this; var c = new q; a && c.mixIn(a); c.hasOwnProperty("init") || (c.init = function () { c.$super.init.apply(this, arguments) }); c.init.prototype = c; c.$super = this; return c }, create: function () { var a = this.extend(); a.init.apply(a, arguments); return a }, init: function () { }, mixIn: function (a) { for (var c in a) a.hasOwnProperty(c) && (this[c] = a[c]); a.hasOwnProperty("toString") && (this.toString = a.toString) }, clone: function () { return this.init.prototype.extend(this) } },
		r = g.WordArray = m.extend({
			init: function (a, c) { a = this.words = a || []; this.sigBytes = c != s ? c : 4 * a.length }, toString: function (a) { return (a || k).stringify(this) }, concat: function (a) { var c = this.words, d = a.words, b = this.sigBytes; a = a.sigBytes; this.clamp(); if (b % 4) for (var e = 0; e < a; e++)c[b + e >>> 2] |= (d[e >>> 2] >>> 24 - 8 * (e % 4) & 255) << 24 - 8 * ((b + e) % 4); else if (65535 < d.length) for (e = 0; e < a; e += 4)c[b + e >>> 2] = d[e >>> 2]; else c.push.apply(c, d); this.sigBytes += a; return this }, clamp: function () {
				var a = this.words, c = this.sigBytes; a[c >>> 2] &= 4294967295 <<
					32 - 8 * (c % 4); a.length = h.ceil(c / 4)
			}, clone: function () { var a = m.clone.call(this); a.words = this.words.slice(0); return a }, random: function (a) { for (var c = [], d = 0; d < a; d += 4)c.push(4294967296 * h.random() | 0); return new r.init(c, a) }
		}), l = f.enc = {}, k = l.Hex = {
			stringify: function (a) { var c = a.words; a = a.sigBytes; for (var d = [], b = 0; b < a; b++) { var e = c[b >>> 2] >>> 24 - 8 * (b % 4) & 255; d.push((e >>> 4).toString(16)); d.push((e & 15).toString(16)) } return d.join("") }, parse: function (a) {
				for (var c = a.length, d = [], b = 0; b < c; b += 2)d[b >>> 3] |= parseInt(a.substr(b,
					2), 16) << 24 - 4 * (b % 8); return new r.init(d, c / 2)
			}
		}, n = l.Latin1 = { stringify: function (a) { var c = a.words; a = a.sigBytes; for (var d = [], b = 0; b < a; b++)d.push(String.fromCharCode(c[b >>> 2] >>> 24 - 8 * (b % 4) & 255)); return d.join("") }, parse: function (a) { for (var c = a.length, d = [], b = 0; b < c; b++)d[b >>> 2] |= (a.charCodeAt(b) & 255) << 24 - 8 * (b % 4); return new r.init(d, c) } }, j = l.Utf8 = { stringify: function (a) { try { return decodeURIComponent(escape(n.stringify(a))) } catch (c) { throw Error("Malformed UTF-8 data"); } }, parse: function (a) { return n.parse(unescape(encodeURIComponent(a))) } },
		u = g.BufferedBlockAlgorithm = m.extend({
			reset: function () { this._data = new r.init; this._nDataBytes = 0 }, _append: function (a) { "string" == typeof a && (a = j.parse(a)); this._data.concat(a); this._nDataBytes += a.sigBytes }, _process: function (a) { var c = this._data, d = c.words, b = c.sigBytes, e = this.blockSize, f = b / (4 * e), f = a ? h.ceil(f) : h.max((f | 0) - this._minBufferSize, 0); a = f * e; b = h.min(4 * a, b); if (a) { for (var g = 0; g < a; g += e)this._doProcessBlock(d, g); g = d.splice(0, a); c.sigBytes -= b } return new r.init(g, b) }, clone: function () {
				var a = m.clone.call(this);
				a._data = this._data.clone(); return a
			}, _minBufferSize: 0
		}); g.Hasher = u.extend({
			cfg: m.extend(), init: function (a) { this.cfg = this.cfg.extend(a); this.reset() }, reset: function () { u.reset.call(this); this._doReset() }, update: function (a) { this._append(a); this._process(); return this }, finalize: function (a) { a && this._append(a); return this._doFinalize() }, blockSize: 16, _createHelper: function (a) { return function (c, d) { return (new a.init(d)).finalize(c) } }, _createHmacHelper: function (a) {
				return function (c, d) {
					return (new t.HMAC.init(a,
						d)).finalize(c)
				}
			}
		}); var t = f.algo = {}; return f
}(Math);
(function (h) {
	for (var s = CryptoJS, f = s.lib, g = f.WordArray, q = f.Hasher, f = s.algo, m = [], r = [], l = function (a) { return 4294967296 * (a - (a | 0)) | 0 }, k = 2, n = 0; 64 > n;) { var j; a: { j = k; for (var u = h.sqrt(j), t = 2; t <= u; t++)if (!(j % t)) { j = !1; break a } j = !0 } j && (8 > n && (m[n] = l(h.pow(k, 0.5))), r[n] = l(h.pow(k, 1 / 3)), n++); k++ } var a = [], f = f.SHA256 = q.extend({
		_doReset: function () { this._hash = new g.init(m.slice(0)) }, _doProcessBlock: function (c, d) {
			for (var b = this._hash.words, e = b[0], f = b[1], g = b[2], j = b[3], h = b[4], m = b[5], n = b[6], q = b[7], p = 0; 64 > p; p++) {
				if (16 > p) a[p] =
					c[d + p] | 0; else { var k = a[p - 15], l = a[p - 2]; a[p] = ((k << 25 | k >>> 7) ^ (k << 14 | k >>> 18) ^ k >>> 3) + a[p - 7] + ((l << 15 | l >>> 17) ^ (l << 13 | l >>> 19) ^ l >>> 10) + a[p - 16] } k = q + ((h << 26 | h >>> 6) ^ (h << 21 | h >>> 11) ^ (h << 7 | h >>> 25)) + (h & m ^ ~h & n) + r[p] + a[p]; l = ((e << 30 | e >>> 2) ^ (e << 19 | e >>> 13) ^ (e << 10 | e >>> 22)) + (e & f ^ e & g ^ f & g); q = n; n = m; m = h; h = j + k | 0; j = g; g = f; f = e; e = k + l | 0
			} b[0] = b[0] + e | 0; b[1] = b[1] + f | 0; b[2] = b[2] + g | 0; b[3] = b[3] + j | 0; b[4] = b[4] + h | 0; b[5] = b[5] + m | 0; b[6] = b[6] + n | 0; b[7] = b[7] + q | 0
		}, _doFinalize: function () {
			var a = this._data, d = a.words, b = 8 * this._nDataBytes, e = 8 * a.sigBytes;
			d[e >>> 5] |= 128 << 24 - e % 32; d[(e + 64 >>> 9 << 4) + 14] = h.floor(b / 4294967296); d[(e + 64 >>> 9 << 4) + 15] = b; a.sigBytes = 4 * d.length; this._process(); return this._hash
		}, clone: function () { var a = q.clone.call(this); a._hash = this._hash.clone(); return a }
	}); s.SHA256 = q._createHelper(f); s.HmacSHA256 = q._createHmacHelper(f)
})(Math);
(function () {
	var h = CryptoJS, s = h.enc.Utf8; h.algo.HMAC = h.lib.Base.extend({
		init: function (f, g) { f = this._hasher = new f.init; "string" == typeof g && (g = s.parse(g)); var h = f.blockSize, m = 4 * h; g.sigBytes > m && (g = f.finalize(g)); g.clamp(); for (var r = this._oKey = g.clone(), l = this._iKey = g.clone(), k = r.words, n = l.words, j = 0; j < h; j++)k[j] ^= 1549556828, n[j] ^= 909522486; r.sigBytes = l.sigBytes = m; this.reset() }, reset: function () { var f = this._hasher; f.reset(); f.update(this._iKey) }, update: function (f) { this._hasher.update(f); return this }, finalize: function (f) {
			var g =
				this._hasher; f = g.finalize(f); g.reset(); return g.finalize(this._oKey.clone().concat(f))
		}
	})
})();

(function () {
	var h = CryptoJS, i = h.lib.WordArray; h.enc.Base64 = {
		stringify: function (b) { var e = b.words, f = b.sigBytes, c = this._map; b.clamp(); for (var b = [], a = 0; a < f; a += 3)for (var d = (e[a >>> 2] >>> 24 - 8 * (a % 4) & 255) << 16 | (e[a + 1 >>> 2] >>> 24 - 8 * ((a + 1) % 4) & 255) << 8 | e[a + 2 >>> 2] >>> 24 - 8 * ((a + 2) % 4) & 255, g = 0; 4 > g && a + 0.75 * g < f; g++)b.push(c.charAt(d >>> 6 * (3 - g) & 63)); if (e = c.charAt(64)) for (; b.length % 4;)b.push(e); return b.join("") }, parse: function (b) {
			var b = b.replace(/\s/g, ""), e = b.length, f = this._map, c = f.charAt(64); c && (c = b.indexOf(c), -1 != c && (e = c));
			for (var c = [], a = 0, d = 0; d < e; d++)if (d % 4) { var g = f.indexOf(b.charAt(d - 1)) << 2 * (d % 4), h = f.indexOf(b.charAt(d)) >>> 6 - 2 * (d % 4); c[a >>> 2] |= (g | h) << 24 - 8 * (a % 4); a++ } return i.create(c, a)
		}, _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
	}
})();

/////////////////////////////////////////////////////////////////////////////////
























/////////////////////////
// BEGINNING OF MIXPANEL WRAPPER

/*
mixpanel_initialized = false;

function MixPanelTrackStart(onSuccess, onFailure)
{
	console.log("MIX PANEL LOADED");
	console.log(typeof onSuccess);
	MixPanelCheck(onSuccess, onFailure);
}

function MixPanelCheck(onSuccess, onFailure)
{
	if (typeof mixpanel.identify == 'function') 
	{
		mixpanel_initialized = true;
		mixpanel.identify(connection.consumerId);
		mixpanel.track("Launch App");

		console.log("MixPanel initialized " + mixpanel_initialized);

		console.log(typeof onSuccess);
		onSuccess(); 

		return;
	}

	setTimeout(function(){MixPanelCheck(onSuccess, onFailure);}.bind(this), 500);
}
*/

//Report to Playfab that reports to ONI.
//Not used at the moment.
function TrackONI(eventKey) {
	if (PlayFabClientSDK.IsClientLoggedIn()) {
		PlayFabClientSDK.ExecuteCloudScript(
			{
				FunctionName: "ReportEvent",
				RevisionSelection: PlayFabRevisionSelection,
				FunctionParameter:
				{
					EventName: eventKey
				}
			},
			function (result, error) {
				if (result != null) {
					logjson("RemoteConnection.VerifyCode Result", result);
				}
				else {
					console.log("Connection to server failed");
				}
			}.bind(this));
	}
	else {
		console.log("Track Event (not logged in): " + eventKey);
	}
}

function TrackEvent(eventKey, functionName) {
	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "ReportONIEvent",
			RevisionSelection: PlayFabRevisionSelection,
			FunctionParameter:
			{
				EventName: eventKey,
				FunctionName: functionName
			}
		},
		function (result, error) {
			if (result != null) {
				logjson("ReportONIEvent result: ", result);
			}
			else {
				console.log("Connection to server failed");
			}
		}.bind(this));
}


// Track Mixpanel
function Track(eventKey, props) {
	logjson("Tracking " + eventKey, props);
	console.log("props type " + typeof props);

	if (!mixpanel_initialized) {
		console.log("NOT INITIALIZED");
		return;
	}

	var fullProps = { "game": connection.gameName };

	logjson("fullProps before", fullProps);

	if (typeof props === "undefined") {
	}
	else if (typeof props === "string" || typeof props === "number") {
		fullProps["value"] = props;
	}
	else {
		for (var key in props) fullProps[key] = props[key];
	}

	logjson("fullProps after", fullProps);

	mixpanel.track(eventKey, fullProps);
}

// END OF MIXPANEL WRAPPER
/////////////////////////

///////////////////////////////////////
// CONNECTION INTERFACE FOR PLAYFAB

/////////////////
// Custom calls

RemoteConnection = function () {
	this.gameName = "";
	this.gameId = "";
	this.serverEnvironment = "";
	this.secretKey = "";
	this.versionNumber = "";

	this.consumerId = "";
};

RemoteConnection.prototype.GetName = function () {
	return "PlayFab Connection";
}

RemoteConnection.prototype.SetProperties = function (properties) {
	console.log("Settings properties");
	//logjson("Properties", properties);

	this.gameName = properties[0];
	this.gameId = BTEConnectionGameID;
	this.serverEnvironment = properties[1];
	this.secretKey = properties[2];
	this.versionNumber = properties[3]

	PlayFab.settings.titleId = this.gameId;
	//PlayFab.settings.developerSecretKey = this.secretKey;
}

//TODO: save player name
//Connects and logs in player
RemoteConnection.prototype.Init = function (consumerId, displayName, onSuccess, onFailure) {
	try {
		console.log(onSuccess);
		console.log("PlayFab doesn't require initialization");

		this.consumerId = consumerId;

		console.log("Logging in user");
		//logjson("", this);

		// Request Login with Custom ID
		var loginRequest =
		{
			CustomId: this.consumerId,
			CreateAccount: true
		};

		logjson("LoginRequest", loginRequest);
		PlayFabClientSDK.LoginWithCustomID(
			loginRequest,
			function (loginResult, loginError) {
				LoginWithCustomIDHandler.bind(this)(loginResult, loginError, onSuccess, onFailure);
			}.bind(this));
	}
	catch (exception) {
		//ReportError(exception);		
		//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
		console.error(exception);
		console.error("Connection Error Dialog Shown");
		this.retryFunction = function () {
			RemoteConnection.prototype.Init.bind(this)(consumerId, displayName, onSuccess, onFailure);
		}.bind(this);
		this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
	}
}

//Uses playfab id to log in
LoginWithCustomIDHandler = function (loginResult, loginError, onSuccess, onFailure) {
	console.log(onSuccess);
	console.log("LoginWithCustomIDHandler");

	logjson("LoginResponse", { result: loginResult, error: loginError });

	if (loginResult) {
		// Get DisplayName through Account Info
		PlayFabClientSDK.GetAccountInfo(
			{},
			function (infoResult, infoError) {
				GetAccountInfoHandler.bind(this)(infoResult, infoError, onSuccess, onFailure);
			}.bind(this));
	}
	else if (onFailure) {
		//onFailure(loginError);		
		//TrackEvent("Connection Error Dialog Shown", "LoginWithCustomIDHandler");
		console.warn("Connection Error Dialog Shown; LoginWithCustomIDHangler");
		this.retryFunction = function () {
			LoginWithCustomIDHandler.bind(this)(loginResult, loginError, onSuccess, onFailure);
		}.bind(this);
		this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
	}
}

//Retreive account data from
GetAccountInfoHandler = function (infoResult, infoError, onSuccess, onFailure) {

	console.log(onSuccess);
	console.log("GetAccountInfoHandler");
	logjson("AccountInfo infoResult", { result: infoResult, error: infoError });

	if (infoResult != null) {
		allResults = {};
		allResults.displayName = infoResult.data.AccountInfo.TitleInfo.DisplayName;
		GetCreditsThroughInventory.bind(this)(allResults, onSuccess, onFailure);
	}
	else if (onFailure != null) {
		onFailure(infoError);
	}
}

GetCreditsThroughInventory = function (allResults, onSuccess, onFailure) {

	console.log(onSuccess);
	console.log("GetCreditsThroughInventory");
	PlayFabClientSDK.GetUserInventory(
		{},
		function (inventoryResult, inventoryError) { GetUserInventoryHandler.bind(this)(inventoryResult, inventoryError, allResults, onSuccess, onFailure); }.bind(this));
}

//Gets player's credits from playfab
GetUserInventoryHandler = function (inventoryResult, inventoryError, allResults, onSuccess, onFailure) {

	console.log(onSuccess);
	console.log("GetUserInventoryHandler");


	if (inventoryResult != null) {
		allResults.gameCredits = inventoryResult.data.VirtualCurrency.GC;
		allResults.offlineCredits = inventoryResult.data.VirtualCurrency.OC;

		logjson("Inventory", { result: inventoryResult, error: inventoryError });

		var parameters =
		{
			FunctionName: "GetOwnedGameToLifeCodes",
			RevisionSelection: PlayFabRevisionSelection
		};

		// Get the Promotional Codes
		//Not used
		PlayFabClientSDK.ExecuteCloudScript(
			parameters,
			function (result, error) {
				GetOwnedGameToLifeCodesHandler.bind(this)(result, error, allResults, onSuccess, onFailure);
			}.bind(this)
		);

	}
	else if (onFailure != null) {
		//onFailure(inventoryError);
		TrackEvent("Connection Error Dialog Shown", "GetUserInventoryHandler");
		this.retryFunction = function () {
			GetUserInventoryHandler.bind(this)(inventoryResult, inventoryError, allResults, onSuccess, onFailure);
		}.bind(this);
		this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
	}
}

GetOwnedGameToLifeCodesHandler = function (result, error, allResults, onSuccess, onFailure) {

	console.log("GetOwnedGameToLifeCodesHandler");

	if (result == null) {
		//ReportError(error.error + "(" + error.code + ")");
		//onFailure("Service unavailable");
		TrackEvent("Connection Error Dialog Shown", "GetOwnedGameToLifeCodesHandler");
		this.retryFunction = function () {
			GetOwnedGameToLifeCodesHandler.bind(this)(result, error, allResults, onSuccess, onFailure);
		}.bind(this);
		this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
	}
	else {
		logjson("PromotionalCodes", { result: result, error: error });

		if (onSuccess != null) {
			console.log("onSuccess != null");


			var ownedGameToLifeCodes = [];//result.data.FunctionResult.Codes;

			if (result.data.Error != null) {
				onFailure(result.data.Error);
			}
			else {
				result.data.FunctionResult.Codes.forEach(
					function (code) {
						ownedGameToLifeCodes.unshift({ code: code.Code, isNew: false });
					}.bind(this)
				);

				console.log(onSuccess);

				allResults.ownedGameToLifeCodes = ownedGameToLifeCodes;

				onSuccess(allResults);
			}
		}
	}
}

//Update stored player name-
ChangeDisplayName = function (displayName, onSuccess, onFailure) {
	console.log("ChangeDisplayName");
	var dataRequest =
	{
		DisplayName: displayName
	};

	PlayFabClientSDK.UpdateUserTitleDisplayName(dataRequest,
		function (loginResult, loginError) {
			logjson("UpdateDisplayNameResponse", { result: loginResult, error: loginError });

			if (loginResult != null) {
				if (onSuccess != null) {
					onSuccess();
				}
			}
			else {
				if (onFailure != null) {
					onFailure(loginError);
				}
			}
		}.bind(this)
	);
}

//Tells playfab to add credits to player
RemoteConnection.prototype.addGameCredits = function (amount, onResponse) {
	var request = {};
	request["eventKey"] = "addGameCredits";
	request["amount"] = amount;
	gamesparks.sendWithData("LogEventRequest", request, onResponse);
}


RemoteConnection.prototype.PostScore = function (score, usState, onSuccess, onFailure) {
	//var globalLeaderboard = "globalLeaderboard." + this.gameName;
	//var localLeaderboard = "localLeaderboard." + this.gameName + "." + usState;
	var globalLeaderboard = "globalLeaderboard.CarnivalGame";
	var localLeaderboard = "localLeaderboard.CarnivalGame." + usState;

	var globalCode = { result: "NONE", message: "" };
	var localCode = { result: "NONE", message: "" };

	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "SetLeaderboardScore",
			RevisionSelection: PlayFabRevisionSelection,
			FunctionParameter:
			{
				score: score,
				leaderboard: globalLeaderboard
			}
		},
		function (globalResult, error) {
			logjson("SetLeaderboardScore (GLOBAL)", { result: globalResult, error: error });

			if (globalResult != null) {
				if (!globalResult.data.Error) {
					globalCode =
						{
							result: globalResult.data.FunctionResult.Result,
							message: globalResult.data.FunctionResult.Message,
							code: globalResult.data.FunctionResult.Code
						};
				}
				//Find out if data is successfuly retieved
				if (onSuccess != null) {
					PlayFabClientSDK.ExecuteCloudScript(
						{
							FunctionName: "SetLeaderboardScore",
							RevisionSelection: PlayFabRevisionSelection,
							FunctionParameter:
							{
								score: score,
								leaderboard: localLeaderboard
							}
						},
						function (localResult, error) {
							logjson("SetLeaderboardScore (LOCAL)", { result: localResult, error: error });

							if (localResult != null) {
								if (!localResult.data.Error) {
									localCode =
										{
											result: localResult.data.FunctionResult.Result,
											message: localResult.data.FunctionResult.Message,
											code: localResult.data.FunctionResult.Code
										};
								}
								if (onSuccess != null) {
									onSuccess({ globalCode, localCode });
								}
							}
							else {
								if (onFailure != null) { onFailure(error); }
							}
						}.bind(this)
					);
				}
			}
			else {
				if (onFailure != null) { onFailure(error); }
			}
		}.bind(this)
	);
}

RemoteConnection.prototype.GetLeaderboardScores = function (usState, onSuccess, onFailure) {
	//var globalLeaderboard = "globalLeaderboard." + this.gameName;
	//var localLeaderboard = "localLeaderboard." + this.gameName + "." + usState;
	var globalLeaderboard = "globalLeaderboard.CarnivalGame";
	var localLeaderboard = "localLeaderboard.CarnivalGame." + usState;

	// Global leaderboards
	var globalRequest =
	{
		StatisticName: globalLeaderboard,
		StartPosition: 0,
		MaxResultsCount: 10,
		UseSpecificVersion: false
	};

	PlayFabClientSDK.GetLeaderboard(globalRequest,
		function (globalResult, globalError) {
			logjson("GetLeaderboard globalLeaderboard", { request: globalRequest, result: globalResult, error: globalError });

			if (globalResult != null) {
				// Local leaderboards
				var localRequest =
				{
					StatisticName: localLeaderboard,
					StartPosition: 0,
					MaxResultsCount: 10,
					UseSpecificVersion: false
				};

				PlayFabClientSDK.GetLeaderboard(localRequest,
					function (localResult, localError) {
						logjson("GetLeaderboard " + localLeaderboard, { request: localRequest, result: localResult, error: localError });

						if (localResult != null) {
							localCode = localResult.data.FunctionResult;

							// Player Global
							var playerGlobalRequest =
							{
								StatisticName: globalLeaderboard,
								MaxResultsCount: 1,
								UseSpecificVersion: false
							};

							PlayFabClientSDK.GetLeaderboardAroundPlayer(playerGlobalRequest,
								function (globalPlayerResult, playerGlobalError) {
									logjson("GetLeaderboard PlayerGlobal", { request: playerGlobalRequest, result: globalPlayerResult, error: playerGlobalError });

									if (globalPlayerResult != null) {
										// Player Local
										var playerLocalRequest =
										{
											StatisticName: localLeaderboard,
											MaxResultsCount: 1,
											UseSpecificVersion: false
										};

										PlayFabClientSDK.GetLeaderboardAroundPlayer(playerLocalRequest,
											function (localPlayerResult, playerLocalError) {
												logjson("GetLeaderboard PlayerLocal", { request: playerLocalRequest, result: localPlayerResult, error: playerLocalError });

												if (localPlayerResult != null) {
													logjson("globalResult", globalResult);
													logjson("localResult", localResult);
													logjson("globalPlayerResult", globalPlayerResult);
													logjson("localPlayerResult", localPlayerResult);

													if (onSuccess != null) {
														//var globalPlayers = [];
														//var localPlayers = [];

														//var globalPlayer = {};
														//var localPlayer = {};

														onSuccess(this.HelperBuildLeaderboardsResponse(globalResult, localResult, globalPlayerResult, localPlayerResult));
													}
												}
												else {
													if (onFailure != null) { onFailure(playerLocalError); }
												}
											}.bind(this)
										);
									}
									else {
										if (onFailure != null) { onFailure(playerGlobalError); }
									}
								}.bind(this)
							);
						}
						else {
							if (onFailure != null) { onFailure(localError); }
						}
					}.bind(this)
				);
			}
			else {
				if (onFailure != null) { onFailure(globalError); }
			}
		}.bind(this)
	);
}

RemoteConnection.prototype.HelperBuildLeaderboardsResponse = function (globalResult, localResult, globalPlayerResult, localPlayerResult) {
	//var defaultPlayerName = this.gameName + "Player";
	var defaultPlayerName = kDefaultPlayerName;

	var response =
	{
		global:
		{
			player: { position: 0, displayName: "", score: 0 },
			topPlayers: []
		},
		local:
		{
			player: { position: 0, displayName: "", score: 0 },
			topPlayers: []
		}
	};

	if (globalResult.data != null && globalResult.data.Leaderboard != null) {
		globalResult.data.Leaderboard.forEach(
			function (player) {
				player.DisplayName = player.DisplayName != null ? player.DisplayName : defaultPlayerName;

				response.global.topPlayers.push(
					{
						position: player.Position + 1,
						displayName: player.DisplayName,
						score: player.StatValue
					});
			}.bind(this));
	}

	if (localResult.data != null && localResult.data.Leaderboard != null) {
		localResult.data.Leaderboard.forEach(
			function (player) {
				player.DisplayName = player.DisplayName != null ? player.DisplayName : defaultPlayerName;

				response.local.topPlayers.push(
					{
						position: player.Position + 1,
						displayName: player.DisplayName,
						score: player.StatValue
					});
			}.bind(this));
	}

	if (globalPlayerResult.data != null && globalPlayerResult.data.Leaderboard != null) {
		var player = globalPlayerResult.data.Leaderboard[0];

		if (player != null) {
			player.DisplayName = player.DisplayName != null ? player.DisplayName : defaultPlayerName;
			response.global.player = { position: player.Position + 1, displayName: player.DisplayName, score: player.StatValue };
		}
		else {
			response.global.player = { position: 0, displayName: "", score: 0 };
		}
	}

	if (localPlayerResult.data != null && localPlayerResult.data.Leaderboard != null) {
		var player = localPlayerResult.data.Leaderboard[0];

		if (player != null) {
			player.DisplayName = player.DisplayName != null ? player.DisplayName : defaultPlayerName;
			response.local.player = { position: player.Position + 1, displayName: player.DisplayName, score: player.StatValue };
		}
		else {
			response.global.player = { position: 0, displayName: "", score: 0 };
		}
	}

	return response;
}

// SLOTS
RemoteConnection.prototype.RequestSlotsResult = function (cost, onSuccess, onFailure) {
	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "RequestSlotsResult",
			RevisionSelection: PlayFabRevisionSelection,
			FunctionParameter:
			{
				clientVersion: this.versionNumber
			}
		},
		function (result, error) {
			logjson("RequestSlotResult ", { result: result, error: error });

			if (result != null) {
				if (result.data.Error != null) {
					if (result.data.Error.Error == "CloudScriptExecutionTimeLimitExceeded") {
						ReportCloudScriptExecutionTimeLimitExceeded(result.data.FunctionName);
					}
					else {
						ReportError(result.data.Error.Error);
					}
					onFailure({ error: "An error occurred, please try again later." });
				}
				else if (result.data.FunctionResult.error == "NotEnoughCredits") {
					if (onFailure != null) {
						onFailure({ error: "NotEnoughCredits" });
					}
				}
				else {
					if (onSuccess != null) {
						onSuccess(result.data.FunctionResult);
					}
				}
			}
			else {
				if (onFailure != null) { onFailure(error); }
			}
		}.bind(this)
	);
}

// SCRATCH
RemoteConnection.prototype.RequestScratchResult = function (cost, onSuccess, onFailure) {
	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "RequestScratchResult",
			RevisionSelection: PlayFabRevisionSelection
		},
		function (result, error) {
			logjson("RequestScratchResult ", { result: result, error: error });

			if (result != null) {
				if (result.data.FunctionResult.error == "NotEnoughCredits") {
					if (onFailure != null) {
						onFailure({ error: "NotEnoughCredits" });
					}
				}
				else {
					if (onSuccess != null) {
						onSuccess(result.data.FunctionResult);
					}
				}
			}
			else {
				if (onFailure != null) { onFailure(error); }
			}
		}.bind(this)
	);
}

// LUCKY WHEEL
RemoteConnection.prototype.RequestLuckyWheelResult = function (cost, onSuccess, onFailure) {
	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "RequestLuckyWheelResult",
			RevisionSelection: PlayFabRevisionSelection,
			FunctionParameter:
			{
				cost: cost
			}
		},
		function (result, error) {
			logjson("RequestLuckyWheelResult ", { result: result, error: error });

			if (result != null) {
				if (onSuccess != null) {
					onSuccess(result.data.FunctionResult);
				}
			}
			else {
				if (onFailure != null) { onFailure(error); }
			}
		}.bind(this)
	);
}

// PROMOTIONAL CODES
RemoteConnection.prototype.VerifyCode = function (code, onSuccess, onFailure) {
	code = code.toUpperCase();
	console.log("RemoteConnection.VerifyCode call");
	console.log("(((( " + code);
	PlayFabClientSDK.ExecuteCloudScript(
		{
			FunctionName: "ClaimGlobalCode",
			RevisionSelection: PlayFabRevisionSelection,
			FunctionParameter:
			{
				Code: code
			}
		},
		function (result, error) {
			if (result != null) {
				logjson("RemoteConnection.VerifyCode Result", result);

				if (onSuccess != null) {
					if (result.data.Error == null) {
						onSuccess({ result: result.data.FunctionResult.Result, message: result.data.FunctionResult.Message, gameCredits: parseInt(result.data.FunctionResult.CreditsAwarded) });
					}
					else {
						if (onFailure != null) { onFailure(result.data.Error.Message); }
					}
				}
			}
			else {
				if (onFailure != null) { onFailure(error); }
			}
		}.bind(this)
	);
}


// END OF CONNECTION INTERFACE
//////////////////////////////

// 





























































// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

var connection = null;

// Constants
var kUrlServer = "http://localhost/highscores/";
var kDefaultPlayerName = "Slots Player";

// Decode url values
var QueryVars = {};
var playerIsRegistered = false;

/////////////////////////////////////
// Plugin class
// *** CHANGE THE PLUGIN ID HERE *** - must match the "id" property in edittime.js
//          vvvvvvvv
cr.plugins_.BTEConnection = function (runtime) {
	this.runtime = runtime;
};

(function () {
	/////////////////////////////////////
	// *** CHANGE THE PLUGIN ID HERE *** - must match the "id" property in edittime.js
	//                            vvvvvvvv
	var pluginProto = cr.plugins_.BTEConnection.prototype;

	/////////////////////////////////////
	// Object type class
	pluginProto.Type = function (plugin) {
		this.plugin = plugin;
		this.runtime = plugin.runtime;
	};

	var typeProto = pluginProto.Type.prototype;

	// called on startup for each object type
	typeProto.onCreate = function () {
	};

	/////////////////////////////////////
	// Instance class
	pluginProto.Instance = function (type) {
		this.type = type;
		this.runtime = type.runtime;

		// any other properties you need, e.g...
		// this.myValue = 0;
	};

	var instanceProto = pluginProto.Instance.prototype;

	// called whenever an instance is created
	instanceProto.onCreate = function () {
		this.os = "android";
		var index = 0;

		console.clear();
		console.log("properties:" + JSON.stringify(this.properties));

		this.initialized = false;

		connection = new RemoteConnection();
		connection.SetProperties(this.properties);

		kUrlServer = this.serverPath;

		this.consumerId = "";
		this.playerId = "";
		this.retailerID = "";
		this.countryCode = "";
		this.isWeb = true;

		this.gameCredits = 0;
		this.offlineCredits = 0;
		this.stateCode = "XX";
		this.countryCode = "XX";
		this.localLeaderboard = {};
		this.globalLeaderboard = {};

		this.mainPlayerDisplayName = "New Player";
		this.ownedGameToLifeCodes = [];

		this.canPlay = false;

		this.wasCheckInSuccessful = 0;
		this.CheckInReward = 0;

		this.lastLeaderboardPostHasCode = false;
		this.lastCodeVerificationResult = { result: "", message: "" };
		this.lastSlotsResult = { credits: 0, prize: false, gameCredits: 0 };
		this.lastScratchResult = { credits: 0, prize: false, gameCredits: 0 };
		this.lastLuckyWheelResult = { credits: 0, prize: false, playAgain: 0, unlucky: 0, gameCredits: 0 };
		console.log("instanceProto - Created");

		//Statistics Variables:
		this.statistics = {};

		// note the object is sealed after this call; ensure any properties you'll ever need are set on the object
	};

	// called whenever an instance is destroyed
	// note the runtime may keep the object after this call for recycling; be sure
	// to release/recycle/reset any references to other objects in this function.
	instanceProto.onDestroy = function () {
	};

	// called when saving the full state of the game
	instanceProto.saveToJSON = function () {
		// return a Javascript object containing information about your object's state
		// note you MUST use double-quote syntax (e.g. "property": value) to prevent
		// Closure Compiler renaming and breaking the save format
		return {
			// e.g.
			//"myValue": this.myValue
		};
	};

	// called when loading the full state of the game
	instanceProto.loadFromJSON = function (o) {
		// load from the state previously saved by saveToJSON
		// 'o' provides the same object that you saved, e.g.
		// this.myValue = o["myValue"];
		// note you MUST use double-quote syntax (e.g. o["property"]) to prevent
		// Closure Compiler renaming and breaking the save format
	};

	// only called if a layout object - draw to a canvas 2D context
	instanceProto.draw = function (ctx) {
	};

	// only called if a layout object in WebGL mode - draw to the WebGL context
	// 'glw' is not a WebGL context, it's a wrapper - you can find its methods in GLWrap.js in the install
	// directory or just copy what other plugins do.
	instanceProto.drawGL = function (glw) {
	};


	// The comments around these functions ensure they are removed when exporting, since the
	// debugger code is no longer relevant after publishing.
	/**BEGIN-PREVIEWONLY**/
	/*
	instanceProto.getDebuggerValues = function (propsections)
	{
		// Append to propsections any debugger sections you want to appear.
		// Each section is an object with two members: "title" and "properties".
		// "properties" is an array of individual debugger properties to display
		// with their name and value, and some other optional settings.
		propsections.push({
			"title": "BTEConnection",
			"properties": [
					{ "name": "gameName", "value": this.gameName, "readonly": true },
					{ "name": "gameId", "value": this.gameId, "readonly": true },
					{ "name": "serverEnvironment", "value": this.serverEnvironment, "readonly": true },
					{ "name": "secretKey", "value": this.secretKey, "readonly": true },
					{ "name": "consumerId", "value": this.consumerId, "readonly": true },
					{ "name": "gameCredits", "value": this.gameCredits, "readonly": true },
				// Each property entry can use the following values:
				// "name" (required): name of the property (must be unique within this section)
				// "value" (required): a boolean, number or string for the value
				// "html" (optional, default false): set to true to interpret the name and value
				//									 as HTML strings rather than simple plain text
				// "readonly" (optional, default false): set to true to disable editing the property
				
				// Example:
				// {"name": "My property", "value": this.myValue}
			]
		});
	};
	
	instanceProto.onDebugValueEdited = function (header, name, value)
	{
		// Called when a non-readonly property has been edited in the debugger. Usually you only
		// will need 'name' (the property name) and 'value', but you can also use 'header' (the
		// header title for the section) to distinguish properties with the same name.
		//if (name === "My property")
		//	this.myProperty = value;
	};
	*/
	/**END-PREVIEWONLY**/

	//////////////////////////////////////
	// Conditions
	function Cnds() { };

	// the example condition
	//Cnds.prototype.MyCondition = function (myparam)
	//{
	//	// return true if number is positive
	//	return myparam >= 0;
	//};

	// ... other conditions here ...
	Cnds.prototype.OnFeedbackMissing = function () {
		return true;
	};

	Cnds.prototype.OnCheckinComplete = function () {
		return true;
	};

	Cnds.prototype.OnServerUpdateGameCredits = function () {
		return true;
	};

	Cnds.prototype.OnNotEnoughCredits = function () {
		return true;
	}

	Cnds.prototype.OnBTEConnectionInitialized = function () {
		return true;
	}

	Cnds.prototype.NoUserLocation = function () {
		return true;
	}

	Cnds.prototype.OnFatalError = function () {
		return true;
	}

	Cnds.prototype.IsGamePlayable = function () {
		return this.canPlay;
	}

	Cnds.prototype.IsWeb = function () {
		return this.isWeb;
	}

	Cnds.prototype.OnRequestSlotsResult = function () {
		return true;
	}

	Cnds.prototype.OnRequestScratchResult = function () {
		return true;
	}

	Cnds.prototype.OnRequestLuckyWheelResult = function () {
		return true;
	}

	Cnds.prototype.OnLeaderboardScores = function () {
		return true;
	}

	Cnds.prototype.OnCodeVerified = function () {
		return true;
	}

	Cnds.prototype.OnLeaderboardPostResponse = function () {
		return true;
	}

	Cnds.prototype.OnMixPanelInitialized = function () {
		return true;
	}

	Cnds.prototype.ClickerDataRecieved = function () {
		return true;
	}

	Cnds.prototype.ClickerNewGame = function () {
		return true;
	}

	Cnds.prototype.ClickerDataError = function () {
		return true;
	}

	Cnds.prototype.retryFunctionQued = function () {
		return true;
	}

	pluginProto.cnds = new Cnds();

	//////////////////////////////////////
	// Actions




	function Acts() { };

	Acts.prototype.SubmitFeedback = function (text) {
		try {
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "RecordFeedback",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{
						userText: text
					}
				},
				function (result, error) {
					if (result != null) {
						logjson("Submit Feedback Result", result);
					}
					else {
						console.log("Connection to server failed");
					}
				}.bind(this));
		} catch (exception) {
			console.error(exception);
		}
	}

	Acts.prototype.QueryFeedback = function () {
		try {
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "FetchFeedback",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{

					}
				},
				function (result, error) {
					if (result != null) {
						logjson("Query Feedback Result", result);
						if(result.data.FunctionResult == "true"){
							console.log("Feedback was not found");//Show feedback dialog
							this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFeedbackMissing, this);
						}else{
							console.log("Feedback exists");
						}
					}
					else {
						console.log("Connection to server failed");
					}
				}.bind(this));
		} catch (exception) {
			console.error(exception);
		}
	}

	Acts.prototype.RecallFailedFunction = function () {
		console.log("Recalling failed function: " + this.retryFunction);
		if (this.retryFunction) {
			TrackONI("Retrying Connection Function");
			this.retryFunction();
			this.retryFunction = undefined; //we ran it already, so should not run it again by accident.
		}
		else {
			console.error("No recall function set.")
		}
	}

	//GET CLICKER DATA
	Acts.prototype.GetClickerData = function () {
		console.log("Get Clicker Data Function called");
		try {
			console.log("Getting Player Clicker Save Data");
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "GetClickerSave",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{

					}
				},
				function (result, error) {
					if (result != null) {
						logjson("Clicker Save Data: ", result);
						if (result.data.FunctionResult != null) {
							clickerSavedScore = parseInt(result.data.FunctionResult.score);
							clickerItemPurchaseCount = JSON.parse(result.data.FunctionResult.purchases);
							clickerItemUpgradeCount = JSON.parse(result.data.FunctionResult.upgrades);
							var saveClear = true;
							for (var x = 0; x < clickerItemPurchaseCount.length; x++) {
								if (clickerItemPurchaseCount[x] != 0 || clickerItemUpgradeCount[x] != 0) {
									saveClear = false;
								}
							}
							if (saveClear) {
								this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.ClickerNewGame, this);
							}
							else {
								this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.ClickerDataRecieved, this);
							}
						} else {
							this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.ClickerNewGame, this);
						}
					}
					else {
						console.error("Connection to server failed, result is null");
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.ClickerDataError, this);
					}
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.ClickerDataError, this);
		}
	}

	Acts.prototype.SaveClickerData = function (score, purchases, upgrades) {
		console.log("Saving Clicker Data Function called");

		clickerSavedScore = score;
		var purchasesArray = purchases.split(",");
		var upgradesArray = upgrades.split(",");
		for (var x = 0; x < purchasesArray.length; x++) {
			clickerItemPurchaseCount[x] = parseInt(purchasesArray[x]);
			clickerItemUpgradeCount[x] = parseInt(upgradesArray[x]);
		}

		try {
			console.log("Saving Player Clicker Save Data");
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "SaveClickerData",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{
						clickerPurchases: JSON.stringify(clickerItemPurchaseCount),
						clickerScore: JSON.stringify(clickerSavedScore),
						clickerUpgrades: JSON.stringify(clickerItemUpgradeCount)
					}
				},
				function (result, error) {
					if (result != null) {
						logjson("Clicker Saved to server: ", result);
					}
					else {
						console.error("Connection to server failed, result is null");
					}
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);
		}
	}


	Acts.prototype.CheckIn = function (latitude, longitude) {
		try {
			console.log("Latitude: " + this.latitude + "Longitude: " + this.longitude);
			console.log("Checking In");
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "CheckIn",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{
						Latitude: this.latitude,
						Longitude: this.longitude
					}
				},
				function (result, error) {
					if (result != null) {
						logjson("RemoteConnection.VerifyCode Result", result);
						console.log("Success");

						if (result.status == "OK") {
							logjson("result.data.FunctionResult", result.data.FunctionResult);
							logjson("result.data", result.data);

							if (result.data.Error != undefined) {
								if (result.data.Error.Error == "CloudScriptExecutionTimeLimitExceeded") {
									ReportCloudScriptExecutionTimeLimitExceeded(result.data.FunctionName);
									TrackEvent("Connection Error Dialog Shown", "CheckIn");

									this.retryFunction = function () { Acts.prototype.CheckIn.bind(this)(latitude, longitude); }.bind(this);
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);

								}
								else if (result.data.Error.Error == "CloudScriptHTTPRequestError") {
									TrackONI("Checkin CloudScriptHTTPRequestError");
									TrackEvent("Connection Error Dialog Shown", "CheckIn");

									this.retryFunction = function () { Acts.prototype.CheckIn.bind(this)(latitude, longitude); }.bind(this);
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);

								}
								else {
									ReportError(result.data.Error.Error);
									this.retryFunction = function () { Acts.prototype.CheckIn.bind(this)(latitude, longitude); }.bind(this);
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
								}

								this.wasCheckInSuccessful = 2;
								this.CheckInReward = 0;
								this.CheckInTextResult = "Could not connect to the server. Please try again later.";

								//We are retrying
								//Do this when we tried more than 3 times				

								//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
							}
							else {
								if (result.data.FunctionResult.Result == "OK") {
									this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
									this.wasCheckInSuccessful = 1;
									this.CheckInTextResult = "You received " + this.CheckInReward + " credits for checking in at a " + BTEConnectionBrandName + " store!";
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
									this.gameCredits = this.gameCredits + this.CheckInReward;
								}
								else if (result.data.FunctionResult.Result == "Error") {
									this.wasCheckInSuccessful = 2;
									this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
									this.CheckInTextResult = result.data.FunctionResult.Error;
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
								}
							}
						}
					}
					else {
						console.error("Connection to server failed, result is null");
						this.wasCheckInSuccessful = 0;
						this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
						//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);

						TrackEvent("Connection Error Dialog Shown", "CheckIn");
						this.retryFunction = function () { Acts.prototype.CheckIn.bind(this)(latitude, longitude); }.bind(this);
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
					}
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);

			this.wasCheckInSuccessful = 0;
			this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
			//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);

			TrackEvent("Connection Error Dialog Shown", "CheckIn");
			this.retryFunction = function () { Acts.prototype.CheckIn.bind(this)(latitude, longitude); }.bind(this);
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.retryFunctionQued, this);
		}
	}

	Acts.prototype.GetDailyCredits = function (latitude, longitude) {
		try {
			console.log("Checking for daily credits");
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "GiveDailyCredits",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{

					}
				},
				function (result, error) {
					if (result != null) {
						logjson("RemoteConnection.VerifyCode Result", result);
						console.log("Success");
						if (result.status == "OK") {
							logjson("result.data.FunctionResult", result.data.FunctionResult);
							logjson("result.data", result.data);
							if (result.data.Error != undefined) {
								if (result.data.Error.Error == "CloudScriptExecutionTimeLimitExceeded") {
									ReportCloudScriptExecutionTimeLimitExceeded(result.data.FunctionName);
								}
								else {
									ReportError(result.data.Error.Error);
								}
								this.wasCheckInSuccessful = 2;
								this.CheckInReward = 0;
								this.CheckInTextResult = "Could not connect to the server. Please try again later.";
								this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
							}
							else {
								if (result.data.FunctionResult.Result == "OK") {
									this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
									this.wasCheckInSuccessful = 1;
									this.CheckInTextResult = "You received " + this.CheckInReward + " credits for logging in to " + GameName + " today! Login tomorrow to receive 100 more credits.";
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
									this.gameCredits = this.gameCredits + this.CheckInReward;
								}
								if (result.data.FunctionResult.Result == "Error") {
									this.wasCheckInSuccessful = 2;
									this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
									this.CheckInTextResult = result.data.FunctionResult.Error;
									this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
								}
							}
						}
					}
					else {
						console.error("Connection to server failed, result is null");
						this.wasCheckInSuccessful = 0;
						this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
					}
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);

			this.wasCheckInSuccessful = 0;
			this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
		}
	}

	Acts.prototype.Initialize = function () {
		if (this.initialized) { return; }

		try {

			//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);

			//return;

			this.initialized = true;

			LoadQueryVars(window);

			var hasID = this.LoadConsumerId();
			var hasName = this.LoadName();
			var hasRetailerID = this.LoadRetailerID();

			/*if (!hasID) {
				ReportError("No consumer ID provided.");
				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
				return;
			}*/


			if (!hasRetailerID) {
				console.error("No retailer ID")

				//Just for now 
				this.retailerID = 8;

				//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);

				//return;
			}

			playerIsRegistered = hasID && hasName;
			this.hasLocation = this.LoadLocation();

			if (!this.hasLocation) {
				console.log("Player doesn't have location");
				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.NoUserLocation, this);
			}

			console.log("Initialize BTEConnection");
			console.log("mainPlayerDisplayName: " + this.mainPlayerDisplayName);
			console.log("Consumer Id: " + this.consumerId);
			console.log("Credits: " + this.gameCredits);
			console.log("Using connection: " + connection.GetName());
			console.log("Init preview " + connection);

			this.gameCredits = 0;

			this.playerId = this.consumerId;


			console.log(this.retailerID);
			console.log("Country Code: " + this.countryCode);
			connection.Init.bind(this)(
				this.consumerId,
				this.mainPlayerDisplayName,

				function (result) {
					console.log("Finished: " + this.mainPlayerDisplayName);

					console.log(this.retailerID);
					console.log("Country Code: " + this.countryCode);

					TrackONI("Game Launched");

					PlayFabClientSDK.ExecuteCloudScript(
						{
							FunctionName: "GiveDailyCredits",
							RevisionSelection: PlayFabRevisionSelection,
							FunctionParameter:
							{
								RetailerID: this.retailerID,
								CountryCode: this.countryCode
							}
						},
						function (result, error) {
							if (result != null) {
								logjson("RemoteConnection.VerifyCode Result", result);
								console.log("Success");
								if (result.status == "OK") {
									logjson("result.data.FunctionResult", result.data.FunctionResult);
									logjson("result.data", result.data);
									if (result.data.Error != undefined) {
										if (result.data.Error.Error == "CloudScriptExecutionTimeLimitExceeded") {
											ReportCloudScriptExecutionTimeLimitExceeded(result.data.FunctionName);
										}
										else {
											ReportError(result.data.Error.Error);
										}
										this.wasCheckInSuccessful = 2;
										this.CheckInReward = 0;
										this.CheckInTextResult = "Could not connect to the server. Please try again later.";
										this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
									}
									else {
										if (result.data.FunctionResult.Result == "OK") {
											this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
											this.wasCheckInSuccessful = 1;
											this.CheckInTextResult = "You received " + this.CheckInReward + " credits for logging in to " + GameName + " today! Login tomorrow to receive 100 more credits.";
											this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
											this.gameCredits = this.gameCredits + this.CheckInReward;
										}
										if (result.data.FunctionResult.Result == "Error") {
											this.wasCheckInSuccessful = 2;
											this.CheckInReward = parseInt(result.data.FunctionResult.CreditsAwarded);
											this.CheckInTextResult = result.data.FunctionResult.Error;
											this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
										}
									}
								}
							}
							else {
								console.error("Connection to server failed, result is null");
								this.wasCheckInSuccessful = 0;
								this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
								this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCheckinComplete, this);
							}
						}.bind(this)
					);

					ChangeDisplayName(
						this.mainPlayerDisplayName,
						function (result) { }.bind(this),
						function (error) { logjson("Error", error); }.bind(this)
					);

					PlayFabClientSDK.GetTitleData(
						{},
						function (result, error) {
							if (result != null) {

							}
							else {
								console.error(error);
							}
						}.bind(this));

					logjson("Final Init Result", result);

					this.gameCredits = result.gameCredits + result.offlineCredits;

					if (result.offlineCredits > 0) {
						log("You received credits while offline!" + result.offlineCredits);
					}

					//this.mainPlayerDisplayName = result.displayName;
					this.ownedGameToLifeCodes = result.ownedGameToLifeCodes;

					logjson("List of ownedGameToLifeCodes", this.ownedGameToLifeCodes);

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnBTEConnectionInitialized, this);
				}.bind(this),

				function (error) {
					//Cannot report error when not logged in
					//ReportError(error);

					console.error("Fatal Error: " + error);

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
				}.bind(this)
			);


			/*
			mixpanel.init("d382eb42c0d69193c484afaa865d6a1b",
			{ 'loaded': 
				function()
				{ 
					MixPanelTrackStart(
						function()
						{
							this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnMixPanelInitialized, this);
						}.bind(this),
						function(){}.bind(this)
					);
				}.bind(this)
			});
			*/
			//this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnMixPanelInitialized, this); //DEBUG
		}
		catch (err) {
			//Cannot report error when not logged in
			//ReportError(error);

			console.error("Fatal Error: " + err);

			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
			//Do something...but what?
		}
	};

	Acts.prototype.GetStatistic = function (statName) {
		if (!(statName in this.statistics)) {
			console.log("Missing " + statName + " element, creating one now...");
			this.statistics[statName] = 0;
		}

		try {
			console.log("Fetching player statistic");
			PlayFabClientSDK.ExecuteCloudScript(
				{
					FunctionName: "GetPlayerStatistic",
					RevisionSelection: PlayFabRevisionSelection,
					FunctionParameter:
					{
						statName: statName,
						value: 0
					}
				},
				function (result, error) {
					if (result != null) {
						logjson("Statistic Result", result);
						console.log("Statistic Value: " + result.data.FunctionResult);
						if (result.data.FunctionResult <= 1) {
							this.statistics[statName] = 0;
						} else {
							this.statistics[statName] = result.data.FunctionResult - 1;
						}
					}
					else {
						console.error("Connection to server failed, result is null");
						this.wasCheckInSuccessful = 0;
						this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
						this.statistics[statName] = 0;
					}
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);
			this.CheckInTextResult = "We couldn't reach our server, please connect to the Internet and try again.";
			this.statistics[statName] = 0;
		}
	}

	/*
	Acts.prototype.UseGameCredits = function (credits)
	{
		if (!this.initialized) { return; }
	
		this.canPlay = false;
	
		console.log("Attempting to use: " + credits + " credits");
		this.UseServerGameCredits(credits);
	}
	
	Acts.prototype.SetGameCredits = function (credits)
	{
		if (!this.initialized) { return; }
	
		this.canPlay = false;
	
		console.log("Attempting to set: " + credits + " credits");
		this.SetServerGameCredits(credits);
	}
	*/
	Acts.prototype.RequestSlotsResult = function (gameCost) {
		try {
			if (!this.initialized) { return; }

			this.canPlay = false;

			connection.RequestSlotsResult(gameCost,
				function (result) {
					logjson("Final Request slots result", result);
					//logjson(result["result"]);

					this.lastSlotsResult.credits = result.gameCreditsWon;
					this.lastSlotsResult.prize = result.prize;// && (result.code.Result === "OK");
					console.log("Prize Output: " + result.prize);
					this.lastSlotsResult.gameCredits = result.gameCreditsAfter;
					this.lastSlotsResult.prizeID = result.prizeID;

					this.gameCredits -= result.cost;
					if (result.code !== undefined && result.code.Result === "OK") {
						this.ownedGameToLifeCodes.unshift(
							{
								code: result.code.Code,
								isNew: true
							}
						);
					}

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnRequestSlotsResult, this);

					this.gameCredits = result.gameCreditsAfter;
				}.bind(this),
				function (error) {
					console.error(error);

					if (error.error == "NotEnoughCredits") {
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnNotEnoughCredits, this);
					}
					else {
						//User doesn't win anything
						console.log("error so win nothing");
						this.lastSlotsResult.credits = 0;
						this.lastSlotsResult.prize = false;// && (result.code.Result === "OK");
						this.lastSlotsResult.prizeID = 0;
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnRequestSlotsResult, this);
					}
				}.bind(this)
			);
		}
		catch (err) {
			ReportError(err);
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
			//Do something...but what?
		}
	}

	Acts.prototype.RequestScratchResult = function (gameCost) {
		if (!this.initialized) { return; }

		this.canPlay = false;

		connection.RequestScratchResult(gameCost,
			function (result) {
				logjson("Final Request scratch result", result);
				logjson("lastScratchResult", this.lastScratchResult);

				this.lastScratchResult.credits = result.gameCreditsWon;
				this.lastScratchResult.prize = result.prize && (result.code.Result === "OK");
				this.lastScratchResult.gameCredits = result.gameCreditsAfter;

				this.gameCredits -= result.cost;

				if (result.code.Result === "OK") {
					this.ownedGameToLifeCodes.unshift(
						{
							code: result.code.Code,
							isNew: true
						}
					);
				}

				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnRequestScratchResult, this);

				this.gameCredits = result.gameCreditsAfter;
			}.bind(this),
			function (error) {
				logjson(error);

				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnNotEnoughCredits, this);
			}.bind(this)
		);
	}

	Acts.prototype.RequestLuckyWheelResult = function (gameCost) {
		if (!this.initialized) { return; }

		this.canPlay = false;

		connection.RequestLuckyWheelResult(gameCost,
			function (result) {
				logjson("Final Request lucky wheel result", result);

				this.lastLuckyWheelResult.credits = result.resultCredits;
				this.lastLuckyWheelResult.prize = result.resultPrize && (result.code.Result === "OK");
				this.lastLuckyWheelResult.playAgain = result.resultPlayAgain;
				this.lastLuckyWheelResult.unlucky = result.resultUnlucky;
				this.lastLuckyWheelResult.gameCredits = result.gameCreditsAfter;

				this.gameCredits -= result.cost;

				if (result.code.Result === "OK") {
					this.ownedGameToLifeCodes.unshift(
						{
							code: result.code.Code,
							isNew: true
						}
					);
				}

				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnRequestLuckyWheelResult, this);

				this.gameCredits = result.gameCreditsAfter;
			}.bind(this),
			function (error) {
				logjson("Error", error);

				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnNotEnoughCredits, this);
			}.bind(this)
		);
	}


	Acts.prototype.PostScore = function (score) {
		try {
			if (!this.initialized) { return; }

			this.canPlay = false;

			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
			connection.PostScore(score, this.stateCode,
				function (response) {
					logjson("Final Post Score", response);

					this.lastLeaderboardPostHasCode = false;

					if (response.globalCode.result === "OK") {
						this.ownedGameToLifeCodes.unshift({ code: response.globalCode.code, isNew: false });
						this.lastLeaderboardPostHasCode = true;
					}

					/*
					if (response.localCode.result === "OK")
					{
						this.ownedGameToLifeCodes.unshift({ code: response.localCode.code, isNew: false });
						this.lastLeaderboardPostHasCode = true;
					}
					*/

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnLeaderboardPostResponse, this);
				}.bind(this),
				function (error) {
					//logjson("Error", error);
					console.error(error);
				}.bind(this)
			);
		}
		catch (err) {
			ReportError(
				{
					Message: "Could not post to leader board. ",
					Error: err
				});
		}
	}

	Acts.prototype.HideCloseButton = function () {
		try {
			if (
				typeof window.webkit !== "undefined" &&
				typeof window.webkit.messageHandlers !== "undefined" &&
				typeof window.webkit.messageHandlers.hideCloseButton !== "undefined" &&
				typeof window.webkit.messageHandlers.hideCloseButton.postMessage !== "undefined") {
				window.webkit.messageHandlers.hideCloseButton.postMessage();
			}

			else if (
				typeof window.Android !== "undefined" &&
				typeof window.Android.hideCloseButton !== "undefined") {
				window.Android.hideCloseButton();
			}
		}
		catch (err) {
			console.error(err);
		}
	}

	Acts.prototype.GetLeaderboardScores = function () {
		try {
			if (!this.initialized) { return; }

			this.canPlay = false;
			connection.GetLeaderboardScores(this.stateCode,
				function (result) {
					logjson("Final Leaderboards result", result);

					this.globalLeaderboard = { player: result.global.player, topPlayers: result.global.topPlayers };
					this.localLeaderboard = { player: result.local.player, topPlayers: result.local.topPlayers };

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnLeaderboardScores, this);
				}.bind(this),

				function (error) {
					ReportError(error);
					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
				}.bind(this)
			);
		}
		catch (err) {
			ReportError(err);
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
		}
	}

	Acts.prototype.SaveDisplayName = function (displayName) {
		try {
			if (!this.initialized) { return; }

			connection.ChangeDisplayName(displayName,
				function (result) {
					this.mainPlayerDisplayName = displayName;
				}.bind(this),
				function (error) {
					console.error(error);
				}.bind(this)
			);
		}
		catch (err) {
			console.error(err);
		}
	}

	Acts.prototype.VerifyCode = function (code) {
		try {
			if (!this.initialized) { return; }

			code = code.toLowerCase();

			/*
			if (!/b-[a-z][a-z]-[a-z][a-z][a-z]-[a-z][a-z]/.test(code))
			{
				this.lastCodeVerificationResult = { result: "ERROR", message: "This is not a valid code." };

				this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCodeVerified, this);

				return;
			}
			*/
			connection.VerifyCode(code,
				function (successResponse) {
					logjson("successResponse", successResponse);

					this.lastCodeVerificationResult = successResponse;

					if (successResponse.result === "OK") {
						this.gameCredits += successResponse.gameCredits;

						this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnServerUpdateGameCredits, this);
					}

					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnCodeVerified, this);

				}.bind(this),
				function (error) {
					ReportError(error);
					this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);

				}.bind(this)
			);
		}
		catch (err) {
			ReportError(err);
			this.runtime.trigger(cr.plugins_.BTEConnection.prototype.cnds.OnFatalError, this);
		}
	}

	// MIXPANEL ANALYTICS
	Acts.prototype.Track = function (eventKey) {
		try {
			TrackONI(eventKey);
		}
		catch (error) {
			console.error(error);
		}
	}

	Acts.prototype.TrackString = function (eventKey, stringValue) {
		try {
			TrackONI(eventKey + ": " + stringValue);
		}
		catch (error) {
			console.error(error);
			console.log("" + eventKey + "; " + stringValue);
		}
	}

	Acts.prototype.TrackNumber = function (eventKey, numberValue) {
		try {
			TrackONI(eventKey + ": " + numberValue);
		}
		catch (error) {
			console.error(error);
		}
	}

	Acts.prototype.TrackJSON = function (eventKey, JSONValue) {
		//try
		//{
		//	TrackONI(eventKey, JSON.parse(JSONValue));
		//}
		//catch(error)
		//{
		console.error("Cannot track JSON");
		//}
	}

	// end of Actions
	//////////////////////////////////////

	pluginProto.acts = new Acts();

	//////////////////////////////////////
	// Expressions
	function Exps() { };

	// the example expression
	//Exps.prototype.CreatePlayerId = function (ret)	// 'ret' must always be the first parameter - always return the expression's result through it!
	//{
	//	// ret.set_int(1337);			// return our value
	//	// ret.set_float(0.5);			// for returning floats
	//	// ret.set_string("Hello");		// for ef_return_string
	//	// ret.set_any("woo");			// for ef_return_any, accepts either a number or string
	//	
	//	var playerId = BTEConfig.gameName + "." + GetDeviceId() + "." + GetRandomNumber();
	//	
	//	ret.set_string(playerId);
	//};
	Exps.prototype.GetKombiColor = function(ret, RGB){
		return ret.set_int(100*(KombiColor[RGB]/256));
	}

	Exps.prototype.GetFullGameName = function (ret){
		return ret.set_string(GameName);
	}

	Exps.prototype.GetLightPosition = function (ret, lightNumber){
		if(lightNumber < 0){
			return ret.set_string("" + LightPosition.length);
		}else{
			return ret.set_string("" + LightPosition[lightNumber][0] + "," + LightPosition[lightNumber][1]);
		}
	}

	Exps.prototype.GetTextColor = function (ret, colorGroup){
		var color = (TextColors[colorGroup][0]) + (256 * TextColors[colorGroup][1]) + (256*256 * TextColors[colorGroup][2]);
		return ret.set_int(color);
	}

	Exps.prototype.BrandName = function (ret) {
		console.log("BRANDNAME: " + BTEConnectionBrandName);
		ret.set_string(BTEConnectionBrandName);
	}

	Exps.prototype.DownloadURL = function (ret) {
		console.log("Download URL: " + BTEConnectionDownloadURL);
		ret.set_string(BTEConnectionDownloadURL);
	}


	Exps.prototype.GetKombi = function (ret) {
		console.log("Kombi Consumer ID: " + this.consumerId);
		console.log("Kombi 0: " + this.consumerId[0]);
		if (this.consumerId[0] == "a") {
			return ret.set_int(0);
		}
		else {
			return ret.set_int(1);
		}
	}

	Exps.prototype.GetPlayerStatistic = function (ret, statName) {
		if (!(statName in this.statistics)) {
			console.log("Missing " + statName + " element, creating one now...");
			this.statistics[statName] = 0;
		}
		return ret.set_int(this.statistics[statName]);
	}

	Exps.prototype.CheckInResult = function (ret) {
		console.log("ChechinResult was = " + this.wasCheckInSuccessful);
		ret.set_int(this.wasCheckInSuccessful);
	}

	Exps.prototype.CheckInTextResult = function (ret) {
		ret.set_string(this.CheckInTextResult);
		/*if(this.wasCheckInSuccessful == 0){
			ret.set_string("Connection to Server Failed");
		}
		if(this.wasCheckInSuccessful == 1){
			ret.set_string("Success!");
		}
		if(this.wasCheckInSuccessful == 2){
			ret.set_string(this.);
		}*/
	}

	var clickerSavedScore = 0;
	Exps.prototype.GetClickerScore = function (ret) {
		ret.set_int(clickerSavedScore); //DEBUG
	}

	var clickerItemPurchaseCount = [];
	Exps.prototype.GetClickerPurchases = function (ret, item) {
		//var itemCount = [1,2,3,4,5,6,7,8];
		ret.set_int(clickerItemPurchaseCount[item]); //DEBUG
	}

	var clickerItemUpgradeCount = [];
	Exps.prototype.GetClickerUpgrades = function (ret, item) {
		//var upgradeLvl = [1,2,3,0,1,2,3,0];
		ret.set_int(clickerItemUpgradeCount[item]); //DEBUG
	}

	Exps.prototype.GetGameCredits = function (ret) {
		ret.set_int(this.gameCredits);
	}

	// Slots results
	Exps.prototype.SlotsResultCredits = function (ret) {
		ret.set_int(this.lastSlotsResult.credits);
	}

	Exps.prototype.SlotsResultPrize = function (ret) {
		ret.set_string(this.lastSlotsResult.prize.toString());
	}

	Exps.prototype.SlotsPrizeId = function (ret) {
		ret.set_int(this.lastSlotsResult.prizeID);
	}

	Exps.prototype.SlotsResultGameCredits = function (ret) {
		ret.set_int(this.lastSlotsResult.gameCredits);
	}


	// Scratch results
	Exps.prototype.ScratchResultCredits = function (ret) {
		ret.set_int(this.lastScratchResult.credits);
	}

	Exps.prototype.ScratchResultPrize = function (ret) {
		ret.set_string(this.lastScratchResult.prize.toString());
	}

	Exps.prototype.ScratchResultGameCredits = function (ret) {
		ret.set_int(this.lastScratchResult.gameCredits);
	}

	// Lucky wheel results
	Exps.prototype.LuckyWheelResultCredits = function (ret) {
		ret.set_int(this.lastLuckyWheelResult.credits);
	}

	Exps.prototype.LuckyWheelResultPrize = function (ret) {
		ret.set_string(this.lastLuckyWheelResult.prize.toString());
	}

	Exps.prototype.LuckyWheelResultPlayAgain = function (ret) {
		ret.set_string(this.lastLuckyWheelResult.playAgain.toString());
	}

	Exps.prototype.LuckyWheelResultUnlucky = function (ret) {
		ret.set_int(this.lastLuckyWheelResult.unlucky);
	}

	// Player ---------

	Exps.prototype.GetPositionForMainPlayer = function (ret, leaderboardType) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["player"]) {
			return ret.set_int(leaderboard["player"]["position"]);
		}

		return ret.set_int(0);
	}

	Exps.prototype.GetNameForMainPlayer = function (ret, leaderboardType) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["player"]) {
			return ret.set_string(leaderboard["player"]["displayName"]);
		}

		return ret.set_string("");
	}

	Exps.prototype.GetScoreForMainPlayer = function (ret, leaderboardType) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["player"]) {
			return ret.set_int(leaderboard["player"]["score"]);
		}

		return ret.set_int(0);
	}

	// Top 10 -------

	Exps.prototype.GetPositionForPlayer = function (ret, leaderboardType, playerIndex) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["topPlayers"] && leaderboard["topPlayers"][playerIndex]) {
			return ret.set_int(leaderboard["topPlayers"][playerIndex]["position"]);
		}

		return ret.set_int(0);
	}

	Exps.prototype.GetNameForPlayer = function (ret, leaderboardType, playerIndex) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["topPlayers"] && leaderboard["topPlayers"][playerIndex]) {
			return ret.set_string(leaderboard["topPlayers"][playerIndex]["displayName"]);
		}

		return ret.set_string("");
	}

	Exps.prototype.GetScoreForPlayer = function (ret, leaderboardType, playerIndex) {
		var leaderboard = leaderboardType == 0 ? this.localLeaderboard : this.globalLeaderboard;

		if (leaderboard && leaderboard["topPlayers"] && leaderboard["topPlayers"][playerIndex]) {
			return ret.set_int(leaderboard["topPlayers"][playerIndex]["score"]);
		}

		return ret.set_int(0);
	}

	Exps.prototype.GetMainPlayerDisplayName = function (ret) {
		ret.set_string(this.mainPlayerDisplayName);
	}

	Exps.prototype.GetMainPlayerUSState = function (ret) {
		ret.set_string(this.stateCode);
	}

	Exps.prototype.GetGameName = function (ret) {
		ret.set_string(connection.gameName);
	}

	Exps.prototype.GetVersionNumber = function (ret) {
		ret.set_string(connection.versionNumber);
	}

	// PROMOTIONAL CODES
	Exps.prototype.GetGameToLifeCodesTotal = function (ret) {
		ret.set_int(this.ownedGameToLifeCodes.length);
	}

	Exps.prototype.GetGameToLifeCode = function (ret, index) {
		ret.set_string(this.ownedGameToLifeCodes[index].code);
	}

	Exps.prototype.GetCodeVerificationResult = function (ret, index) {
		ret.set_string(this.lastCodeVerificationResult.result);
	}

	Exps.prototype.GetCodeVerificationMessage = function (ret, index) {
		ret.set_string(this.lastCodeVerificationResult.message);
	}

	Exps.prototype.IsGameToLifeCodeNew = function (ret, index) {
		ret.set_string(this.ownedGameToLifeCodes[index].isNew.toString());
	}

	Exps.prototype.LastLeaderboardPostHasCode = function (ret) {
		ret.set_string(this.lastLeaderboardPostHasCode.toString());
	}

	//AddExpression(
	//	403,
	//	ef_return_string,
	//	"Get the result for the last code verification, that is OK, CLAIMED, INVALID, EMPTY or ERROR",
	//	kPromotionalCodesCategory,
	//	"GetCodeVerificationResult",
	//	"Returns the result for the last code verification");
	//
	//AddExpression(
	//	404,
	//	ef_return_string,
	//	"Get the result message for the last code verification, this is a descriptive message",
	//	kPromotionalCodesCategory,
	//	"GetCodeVerificationResultMessage",
	//	"Returns the result message for the last code verification");

	// ... other expressions here ...
	// end of Expressions
	//////////////////////////////////////

	pluginProto.exps = new Exps();

	instanceProto.GetDeviceId = function () {
		//return GeRandomNumber();
		//return -1;
		return "" + this.RandomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') + "_" + (new Date).getTime();
		//ADD EPOCH TIME
	}

	instanceProto.GetRandomNumber = function () {
		var result = "";
		for (var i = 0; i < 12; i++) {
			result += int(Math.random() * 10);
		}

		return result;
	}

	instanceProto.RandomString = function (length, chars) {
		var result = '';

		for (var i = length; i > 0; --i) {
			result += chars[Math.floor(Math.random() * chars.length)]
		};

		return result;
	}

	instanceProto.GetURLParameter = function (name) {
		return decodeURIComponent(
			(new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}
	instanceProto.LoadConsumerId = function () {
		if ("consumerId" in QueryVars) {
			this.consumerId = QueryVars["consumerId"];
			this.isWeb = false;
			return true;
		}

		var consumerId = this.GetCookie("consumerId");
		this.isWeb = true;

		if (consumerId == "") {
			consumerId = this.GetDeviceId();
			//consumerId = this.GetURLParameter("consumerId");

			this.SetCookie("consumerId", consumerId);
		}

		this.consumerId = consumerId;
		return false;
	}

	instanceProto.LoadName = function () {
		if ("name" in QueryVars) {
			this.mainPlayerDisplayName = QueryVars["name"];

			return true;
		}

		var name = this.GetCookie("name");

		if (name == "") {
			name = kDefaultPlayerName;
			//consumerId = this.GetURLParameter("consumerId");

			this.SetCookie("name", name);
		}

		this.mainPlayerDisplayName = name;
		return false;
	}

	instanceProto.LoadRetailerID = function () {
		if ("retailerId" in QueryVars) {
			this.retailerID = QueryVars["retailerId"];

			return true;
		}

		return false; //Dont bother with cookies...cannot play the game without the data in the URL
	}

	instanceProto.LoadLocation = function () {
		if ("latitude" in QueryVars) {
			this.latitude = QueryVars["latitude"];
		}
		else {
			return false;
		}

		if ("longitude" in QueryVars) {
			this.longitude = QueryVars["longitude"];
		}
		else {
			return false;
		}

		if ("adminArea" in QueryVars) {
			this.stateCode = QueryVars["adminArea"];
		}
		else {
			return false;
		}

		if ("countryCode" in QueryVars) {
			this.countryCode = QueryVars["countryCode"];
			if (this.countryCode == "US") {
				//Do Nothing
			} else if (this.countryCode == "CA") {
				this.stateCode = "Canada";
			} else {
				this.stateCode = "XX";
				return false;
			}
		}
		else {
			return false;
		}

		return true;
	}

	instanceProto.SetCookie = function (name, value) {
		document.cookie = name + "=" + value + "; path=/";
	}

	instanceProto.GetCookie = function (name) {
		var nameEQ = name + "=";
		var cookies = document.cookie.split(';');

		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i];

			while (cookie.charAt(0) == ' ') {
				cookie = cookie.substring(1, cookie.length);
			}

			if (cookie.indexOf(nameEQ) == 0) {
				return cookie.substring(nameEQ.length, cookie.length);
			}
		}

		return "";
	}

	//////////////////////////////////////
	// GameSparks	
	instanceProto.onGameSparksNonce = function (nonce) {
		//TODO: security hole
		return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(nonce, "bqsLB43dCL3SNXb4mRxfGVCXhRNA9Sxe"));
	}
	// end of GameSparks
	//////////////////////////////////////	
}());

////////////////////////
// LOADING
function LoadQueryVars(window) {
	console.log("LoadQueryVars");

	var result = {};

	var query = window.location.search.substring(1);
	var pairs = query.split("&");

	for (var i = 0; i < pairs.length; i++) {
		var pair = pairs[i].split("=");
		result[pair[0]] = decodeURIComponent(pair[1]);
	}

	logjson("QueryVars", result);

	QueryVars = result;
}


//////////////////////////
// HELPERS

function logjson(json) {
	console.log(JSON.stringify(json, null, 4));
}

function logjson(title, json) {
	if (title === "") {
		console.log(JSON.stringify(json, null, 4));
	}
	else {
		console.log(title);
		console.log(JSON.stringify(json, null, 4));
	}
}

function ReportError(errorMessage) {
	try {
		console.error(errorMessage);
		Rollbar.error(errorMessage);
		TrackONI("Error", errorMessage);
	}
	catch (err) {
		//Do nothing
	}
}

function ReportCloudScriptExecutionTimeLimitExceeded(functionName) {
	Rollbar.error("CloudScriptExecutionTimeLimitExceeded: " + functionName);
}

// Locations for US States
var getDistance = function (location1, location2) {
	var radius = 6371.0; // km
	var dLat = (location2.latitude - location1.latitude) * Math.PI / 180.0;
	var dLon = (location2.longitude - location1.longitude) * Math.PI / 180.0;
	var lat1 = location1.latitude * Math.PI / 180.0;
	var lat2 = location2.latitude * Math.PI / 180.0;
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);

	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = radius * c;

	return d;
}

var findState = function (myLocation) {
	var closestLocation = findClosest(locations, myLocation);

	return closestLocation.state;
}

var findClosest = function (locations, myLocation) {
	var closestLocation = locations[0];
	var closestDistance = getDistance(locations[0], myLocation);

	for (var i = 1; i < locations.length; i++) {
		var distance = getDistance(locations[i], myLocation);

		if (distance < closestDistance) {
			closestDistance = distance;
			closestLocation = locations[i];
		}
	}

	return closestLocation;
}

var isInHotzone = function (locations, myLocation) {
	var threshold = 0.5; //km

	for (var i = 0; i < locations.length; i++) {
		var distance = getDistance(locations[i], myLocation);

		if (distance < threshold) {
			return true;
		}
	}

	return false;
}

var locations = [{ "lattitude": 38.8607539, "longitude": -86.4912307, "state": "IN" }, { "lattitude": 39.2234241, "longitude": -85.8758449, "state": "IN" }];
